# How to copy a customer managed key backup?

[back to Next-Generation Cloud][home]

## Table of Contents
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Table of Contents](#table-of-contents)
- [Introduction](#introduction)
- [Copy Backup using the Console](#copy-backup-using-the-console)
- [Copy Backup using Command Line Interface](#copy-backup-using-command-line-interface)

<!-- /TOC -->

## Introduction
Here we focus on Customer managed (*boot and block*) volume backups and how to copy them to a different region.

When you create a volume optionally, you can encrypt the data in this volume using your own Vault encryption key. If you do so each backup will be automatically also be encrypted using your own Vault encryption key.
You can copy this backup to a different region if you provide the OCID of yor own Vault encryption key in the new region. Therefor you have to [replicate your vault/key](https://docs.oracle.com/en-us/iaas/Content/KeyManagement/Tasks/replicatingvaults.htm) for virtual private vaults or [backup](https://docs.oracle.com/en-us/iaas/Content/KeyManagement/Tasks/backingupvaultsandkeys_topic-To_back_up_a_vault.htm), [replicate](https://docs.oracle.com/en-us/iaas/Content/Object/Tasks/usingreplication.htm#Using_Replication) and [restore](https://docs.oracle.com/en-us/iaas/Content/KeyManagement/Tasks/backingupvaultsandkeys_topic-To_restore_a_vault.htm) the vault for non virtual private vaults. Finally your vault is available in two regions.
In addition [Import your own Key in OCI Vault with Cloud Console (UI)](https://blogs.oracle.com/coretec/post/import-your-own-key-in-oci-vault-with-cloud-console-ui) might help also.


## Copy Backup Using the Console

Let me show you how easy you can create this Server with Resilience by default.

| action                                                    | screenshot                                                                                                                |
| --------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| Create block volume (using your own Vault encryption key) | <img alt="Create block volume" src="../doc/images/copy.backup.1.png" title="Create compute instance" width="100%">    |
| Create a backup of your block volume                      | <img alt="Create a backup" src="../doc/images/copy.backup.2.png" title="Create compute instance" width="100%">    |
| replicate your Vault to the new region                    | see [Introduction](#introduction)                                                                                         |
| Copy your backup to a different region (step 1)           | <img alt="Copy your backup to a different region" src="../doc/images/copy.backup.3.png" title="Create compute instance" width="100%"> |
| Copy your backup to a different region (step 2)           | <img alt="Copy your backup to a different region" src="../doc/images/copy.backup.4.png" title="Create compute instance" width="100%"> |
| list backup in source region (e.g. Frankfurt)             | <img alt="list backup in source region" src="../doc/images/copy.backup.5.png" title="Create compute instance" width="100%"> |
| list backup in target region (e.g. London)                | <img alt="list backup in target region" src="../doc/images/copy.backup.6.png" title="Create compute instance" width="100%"> |

## Copy Backup using Command Line Interface

You can automate this, using OCI [Command Line Interface (CLI)](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm) also. You need [functions0.sh](doc/functions0.sh), [parameter.sh](doc/parameter.sh), [run.remote.region.A.sh](doc/run.remote.region.A.sh), [run.remote.region.B.sh](doc/run.remote.region.B.sh), [showcase.bv.1.sh](doc/showcase.bv.1.sh) on your server. ssh to this server and run /home/opc/showcase.bv.1.sh help and find
- prerequisites
- run your showcase (I of II - this will run ~ 10 minutes)
- see outcome of your showcase
- run your showcase (II of II - this will run ~ 2 minutes)
- reuse the showcase

~~~
/home/opc/showcase.bv.1.sh help
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
showcase 1 (showcase.bv.1.sh) demonstrate block volume backup copy to a different region in a customer managed key environment
   we showcase a: cli management of several customer managed key protected OCI resources (e.g. instance, block volume, block volume backup)
   we showcase b: cli based customer managed key protected block volume creation, attachement to instance, create filesystem
   we showcase c: cli based customer managed key protected block volume backup
   we showcase d: cli based customer managed key protected block volume backup copy to a different region
   we showcase e: cli based customer managed key protected instance using Cloud-Init to run custom scripts or provide custom Cloud-Init configuration
   we showcase f: cli based customer managed key protected instance using Consistent device paths fot attached block volumes


  prerequisites:
  (1) set up your operate instance (e.g. Installing the CLI on Oracle Linux 9 https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm#InstallingCLI__oraclelinux9)
  (2) copy e.g. /home/opc/.ssh/id_rsa.pub (see PUBLIC_SSH_KEY in parameter.sh) to the operate instance (this public key will be used to launch new instances
  (3) copy id_rsa (private key) to your operate server in the opc home/.ssh (/home/opc/.ssh/...)
  (4) prepare your compartment (see COMPARTMENT_OCID in parameter.sh)
  (5) prepare your VCN in Region A and Region B
  (6) prepare your vault in Region A and Region B (see https://blogs.oracle.com/coretec/post/import-your-own-key-in-oci-vault-with-cloud-console-ui)
  (7) copy run.remote.sh, functions0.sh, parameter.sh and showcase.bv.1.sh to your operate server in the opc home (/home/opc/...)
  (8) prepare your parameter.sh
  (9) be aware that this showcase will create new resources in your tenant (in the COMPARTMENT_OCID only)

  run your showcase (I of II - this will run ~ 10 minutes):
  (1) ssh to your operate server
  (2)    show help: /home/opc/showcase.bv.1.sh help
  (3) run showcase: /home/opc/showcase.bv.1.sh

  see outcome of your showcase:
  (1) connect to the OCI UI (e.g. https://cloud.oracle.com/?tenant=<your_tenant_name>&provider=OracleIdentityCloudService
  (2) find your Instance and validate your Encryption key in the boot volume in Region A and Region B
  (3) ssh into Instance in Region A and Region B and check the log (cat /home/opc/bv1/new-bv.log) in the attached block volume
  (4) find your Block Volume and validate your Encryption key in Region A and Region B
  (5) find your Block Volume Backup and validate your Encryption key in Region A and Region B

  run your showcase (II of II - this will run ~ 2 minutes):
  (1) Please validate all showcase resources. If you are ready - please type delete here and we will delete all resources for you

  reuse the showcase:
  (1) analyse and understand the showcase
  (2) make use of it in your environment
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

~~~

### Summary

[Best practices framework for Oracle Cloud Infrastructure](https://docs.oracle.com/en/solutions/oci-best-practices/index.html) recommends to
- [Backup Data in Storage Services](https://docs.oracle.com/en/solutions/oci-best-practices/back-your-data1.html)
- [Encrypt Data in Block Volumes by using keys that you own, and you can manage the keys by using the Oracle Cloud Infrastructure Vault service](https://docs.oracle.com/en/solutions/oci-best-practices/protect-data-rest1.html)
- [Replicate Your Data for Disaster Recovery](https://docs.oracle.com/en/solutions/oci-best-practices/back-your-data1.html)

Now you know how to do this.

<!--- Links -->

[home]:                        https://gitlab.com/hmielimo/next-generation-cloud

<!-- /Links -->
