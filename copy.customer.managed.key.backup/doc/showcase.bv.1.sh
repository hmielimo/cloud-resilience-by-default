#!/bin/bash
#
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
#@  -------------------------------------------------------------------------------------------------------------------------------------------------------------------
#@
#@  showcase 1 (showcase.bv.1.sh) demonstrate block volume backup copy to a different region in a customer managed key environment
#@     we showcase a: cli management of several customer managed key protected OCI resources (e.g. instance, block volume, block volume backup)
#@     we showcase b: cli based customer managed key protected block volume creation, attachment to instance, create filesystem
#@     we showcase c: cli based customer managed key protected block volume backup
#@     we showcase d: cli based customer managed key protected block volume backup copy to a different region
#@     we showcase e: cli based customer managed key protected instance using Cloud-Init to run custom scripts or provide custom Cloud-Init configuration
#@     we showcase f: cli based customer managed key protected instance using Consistent device paths fot attached block volumes
#@    
#@    
#@    prerequisites:
#@    (1) set up your operate instance (e.g. Installing the CLI on Oracle Linux 9 https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm#InstallingCLI__oraclelinux9)
#@    (2) copy e.g. /home/opc/.ssh/id_rsa.pub (see PUBLIC_SSH_KEY in parameter.sh) to the operate instance (this public key will be used to launch new instances
#@    (3) copy id_rsa (private key) to your operate server in the opc home/.ssh (/home/opc/.ssh/...)
#@    (4) prepare your compartment (see COMPARTMENT_OCID in parameter.sh)
#@    (5) prepare your VCN in Region A and Region B
#@    (6) prepare your vault in Region A and Region B (see https://blogs.oracle.com/coretec/post/import-your-own-key-in-oci-vault-with-cloud-console-ui)
#@    (7) copy run.remote.sh, functions0.sh, parameter.sh and showcase.bv.1.sh to your operate server in the opc home (/home/opc/...)
#@    (8) prepare your parameter.sh
#@    (9) be aware that this showcase will create new resources in your tenant (in the COMPARTMENT_OCID only)
#@    
#@    run your showcase (I of II - this will run ~ 10 minutes):
#@    (1) ssh to your operate server
#@    (2)    show help: /home/opc/showcase.bv.1.sh help
#@    (3) run showcase: /home/opc/showcase.bv.1.sh
#@    
#@    see outcome of your showcase:
#@    (1) connect to the OCI UI (e.g. https://cloud.oracle.com/?tenant=<your_tenant_name>&provider=OracleIdentityCloudService
#@    (2) find your Instance and validate your Encryption key in the boot volume in Region A and Region B
#@    (3) ssh into Instance in Region A and Region B and check the log (cat /home/opc/bv1/new-bv.log) in the attached block volume
#@    (4) find your Block Volume and validate your Encryption key in Region A and Region B
#@    (5) find your Block Volume Backup and validate your Encryption key in Region A and Region B
#@    
#@    run your showcase (II of II - this will run ~ 2 minutes):
#@    (1) Please validate all showcase resources. If you are ready - please type delete here and we will delete all resources for you
#@    
#@    reuse the showcase:
#@    (1) analyse and understand the showcase
#@    (2) make use of it in your environment
#@    
#@  -------------------------------------------------------------------------------------------------------------------------------------------------------------------
#
# Update history:
#
# V 1.0.0 09.08.2023 initial version
#


# ---------------------------------------------------------------------------------------------------------------------------------------------
# prepare environement (load functions)
# ---------------------------------------------------------------------------------------------------------------------------------------------
source $(dirname "$0")/functions0.sh
myhelp=no
myhelp=${1}
if [ -z ${myhelp} ] ; then
  echo "no help needed"
else
  if [ ${myhelp} = "help" ] || [ ${myhelp} = "-help" ]; then # show help
    DisplayHelp $(dirname "$0")/showcase.bv.1.sh
    exit
  fi
fi

# ---------------------------------------------------------------------------------------------------------------------------------------------
# Oracle Cloud Infrastructure CLI Command Reference - https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/
# ---------------------------------------------------------------------------------------------------------------------------------------------

MYOUTPUT="showcase 1 demonstrate block volume backup copy to a different region in a customer managed key environment" && MYCOUNT=$((1)) 
if [ 1 -eq 1 ] ; then # start showcase 1
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"
echo "${LogPF1}"                                                                                                              > "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF2} showcase 1 demonstrate block volume backup copy to a different region in a customer managed key environment" >> "${LOG_FILE}"
echo "${LogPF2} $(date "+%d.%m.%Y %H:%M:%S") "                                                                               >> "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF1}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"

if [ 1 -eq 1 ] ; then # check and if needed create Block Volume in Region A
  get_block_volume_OCID "A"
  if [ -z ${REGION_A_BLOCK_VOLUME_OCID} ] ; then # REGION_A_BLOCK_VOLUME_OCID is NULL
    color_print "${MYcolor}" "${PF1} create block volume ${REGION_A_BLOCK_VOLUME_NAME_NAME}"
    create_block_volume "A"
  fi 
  if [ -z ${REGION_A_BLOCK_VOLUME_OCID} ] ; then # REGION_A_BLOCK_VOLUME_OCID is NULL
    color_print "${MYcolor}" "${PF1} create block volume ${REGION_A_BLOCK_VOLUME_NAME} failed"
  fi
  echo "${LogPF2}"                                                                                        >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
  echo "${LogPF2} created block volume ${REGION_A_BLOCK_VOLUME_NAME} in ${REGION_A_PROFILE}"              >> "${LOG_FILE}"
  echo "${LogPF2} - use customer managed key protect ${REGION_A_BLOCK_VOLUME_NAME}"                       >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
fi

if [ 1 -eq 1 ] ; then # check and if needed create Instance in Region A
  get_instance_OCID "A"
  if [ -z ${REGION_A_INSTANCE_OCID} ] ; then # REGION_A_INSTANCE_OCID is NULL
    color_print "${MYcolor}" "${PF1} create Instance ${REGION_A_INSTANCE_NAME}"
    create_instance "A"
  fi
  if [ -z ${REGION_A_INSTANCE_OCID} ] ; then # REGION_A_INSTANCE_OCID is NULL
    color_print "${MYcolor}" "${PF1} create Instance ${REGION_A_INSTANCE_NAME} failed"
  fi
  echo "${LogPF2}"                                                                                        >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
  echo "${LogPF2} created instance ${REGION_A_INSTANCE_NAME} in ${REGION_A_PROFILE}"                      >> "${LOG_FILE}"
  echo "${LogPF2} - use customer managed key protect ${REGION_A_INSTANCE_NAME}"                           >> "${LOG_FILE}"
  echo "${LogPF2} - use Cloud-Init to run custom script in instance ${REGION_A_INSTANCE_NAME}"            >> "${LOG_FILE}"
  echo "${LogPF2} - attached block volume ${REGION_A_BLOCK_VOLUME_NAME} to instance"                      >> "${LOG_FILE}"
  echo "${LogPF2} - created filesystem (ext4) in block volume ${REGION_A_BLOCK_VOLUME_NAME}"              >> "${LOG_FILE}"
  echo "${LogPF2} - log actions in OS filesystem and block volume filesystem"                             >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
fi

if [ 1 -eq 1 ] ; then # check and if needed create Block Volume Backup in Region A
  get_block_volume_backup_OCID "A"
  if [ -z ${REGION_A_BLOCK_VOLUME_BACKUP_OCID} ] ; then # REGION_A_BLOCK_VOLUME_BACKUP_OCID is NULL
    color_print "${MYcolor}" "${PF1} create Instance ${REGION_A_BLOCK_VOLUME_NAME}"
    create_block_volume_backup "A"
    get_block_volume_backup_OCID "A"
  fi
  if [ -z ${REGION_A_BLOCK_VOLUME_BACKUP_OCID} ] ; then # REGION_A_BLOCK_VOLUME_BACKUP_OCID is NULL
    color_print "${MYcolor}" "${PF1} create block volume backup Backup-${REGION_A_BLOCK_VOLUME_NAME} failed"
  fi
  echo "${LogPF2}"                                                                                        >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
  echo "${LogPF2} created backup of ${REGION_A_BLOCK_VOLUME_NAME} in ${REGION_A_PROFILE}"                 >> "${LOG_FILE}"
  echo "${LogPF2} - use customer managed key protect ${REGION_A_BLOCK_VOLUME_BACKUP_NAME}"                >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
fi

if [ 1 -eq 1 ] ; then # check and if needed copy Block Volume Backup from Region A to Region B
  get_block_volume_backup_OCID "B"
  if [ -z ${REGION_B_BLOCK_VOLUME_BACKUP_OCID} ] ; then # REGION_B_BLOCK_VOLUME_BACKUP_OCID is NULL
    color_print "${MYcolor}" "${PF1} copy block volume backup Backup-${REGION_B_BLOCK_VOLUME_NAME} to ${REGION_B_PROFILE}"
    copy_block_volume_backup "A" "B"
    get_block_volume_backup_OCID "B"
  fi
  if [ -z ${REGION_B_BLOCK_VOLUME_BACKUP_OCID} ] ; then # REGION_B_BLOCK_VOLUME_BACKUP_OCID is NULL
    color_print "${MYcolor}" "${PF1} create block volume backup Backup-${REGION_B_BLOCK_VOLUME_NAME} failed"
  fi
  echo "${LogPF2}"                                                                                                       >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                       >> "${LOG_FILE}"
  echo "${LogPF2} created backup copy of ${REGION_A_BLOCK_VOLUME_NAME} in ${REGION_A_PROFILE} to ${REGION_B_PROFILE}"    >> "${LOG_FILE}"
  echo "${LogPF2} - use customer managed key protect ${REGION_B_BLOCK_VOLUME_BACKUP_NAME} in region ${REGION_B_PROFILE}" >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                       >> "${LOG_FILE}"
fi

if [ 1 -eq 1 ] ; then # check and if needed create Instance in Region B
  get_instance_OCID "B"
  if [ -z ${REGION_B_INSTANCE_OCID} ] ; then # REGION_B_INSTANCE_OCID is NULL
    color_print "${MYcolor}" "${PF1} create Instance ${REGION_A_INSTANCE_NAME}"
    create_instance "B"
  fi
  if [ -z ${REGION_B_INSTANCE_OCID} ] ; then # REGION_B_INSTANCE_OCID is NULL
    color_print "${MYcolor}" "${PF1} create Instance ${REGION_B_INSTANCE_NAME} failed"
  fi
  echo "${LogPF2}"                                                                                                         >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                         >> "${LOG_FILE}"
  echo "${LogPF2} created instance ${REGION_B_INSTANCE_NAME} in ${REGION_B_PROFILE}"                                       >> "${LOG_FILE}"
  echo "${LogPF2} - use customer managed key protect ${REGION_B_INSTANCE_NAME}"                                            >> "${LOG_FILE}"
  echo "${LogPF2} - use Cloud-Init to run custom script in instance ${REGION_B_INSTANCE_NAME}"                             >> "${LOG_FILE}"
  echo "${LogPF2} - restore block volume ${REGION_B_BLOCK_VOLUME_NAME} out of backup ${REGION_B_BLOCK_VOLUME_BACKUP_NAME}" >> "${LOG_FILE}"
  echo "${LogPF2} - attached block volume ${REGION_B_BLOCK_VOLUME_NAME} to instance"                                       >> "${LOG_FILE}"
  echo "${LogPF2} - validate log in OS filesystem and block volume filesystem"                                             >> "${LOG_FILE}"
  echo "${LogPF2} - proofed that ${REGION_A_BLOCK_VOLUME_NAME} data successfull show up in ${REGION_B_PROFILE}"            >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                         >> "${LOG_FILE}"
fi

if [ 1 -eq 1 ] ; then # delete all resources in Region A and Region B
color_print "${IGreen}" "**********************************************************************************************************************"
color_print "${IGreen}" "**********************************************************************************************************************"
color_print "${IGreen}" ">>> "
color_print "${IGreen}" ">>> "
color_print "${IGreen}" ">>> "
read -p '>>> Please validate all showcase resources. If you are ready - please type delete here and we will delete all resources for you: ' configserver
if [ "${configserver}" == "delete" ] ; then
  delete_instance "A"
  delete_instance "B"
  delete_block_volume "A"
  delete_block_volume "B"
  delete_block_volume_backup "A"
  delete_block_volume_backup "B"
  echo "${LogPF2}"                                                                                        >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
  echo "${LogPF2} delete all resources"                                                                   >> "${LOG_FILE}"
  echo "${LogPF2} - delete ${REGION_A_INSTANCE_NAME} in ${REGION_A_PROFILE}"                              >> "${LOG_FILE}"
  echo "${LogPF2} - delete ${REGION_B_INSTANCE_NAME} in ${REGION_B_PROFILE}"                              >> "${LOG_FILE}"
  echo "${LogPF2} - delete ${REGION_A_BLOCK_VOLUME_NAME} in ${REGION_A_PROFILE}"                          >> "${LOG_FILE}"
  echo "${LogPF2} - delete ${REGION_B_BLOCK_VOLUME_NAME} in ${REGION_B_PROFILE}"                          >> "${LOG_FILE}"
  echo "${LogPF2} - delete ${REGION_A_BLOCK_VOLUME_BACKUP_NAME} in ${REGION_A_PROFILE}"                   >> "${LOG_FILE}"
  echo "${LogPF2} - delete ${REGION_B_BLOCK_VOLUME_BACKUP_NAME} in ${REGION_B_PROFILE}"                   >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
fi

fi

fi # end of showcase 1

MYOUTPUT="End of Programm" && MYCOUNT=$(($MYCOUNT + 1)) 
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF1}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF2} end of showcase 1 "                                                                                          >> "${LOG_FILE}"
echo "${LogPF2} $(date "+%d.%m.%Y %H:%M:%S") "                                                                               >> "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF1}"                                                                                                             >> "${LOG_FILE}"

# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------
