#!/bin/bash
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
# Update history:
#
# V 1.0.0 15.08.2023 initial version
#
OSlog=/home/opc/new-bv.log        # logfile to log block volume init to OS filesystem
BVlog=/home/opc/bv1/new-bv.log    # logfile to log to block volume filesystem

echo "attach, mount bv1 - size 50G \$(date "+%d.%m.%Y %H:%M:%S")" > ${OSlog}
echo "---------------------------------------------------------------" >> ${OSlog}
echo "validate if bv is attached?"                                     >> ${OSlog}
for i in {1..10}
do
  echo "${i}. sudo oci-iscsi-config show"
  sleep 4
  if [ 1 -eq $( sudo oci-iscsi-config show --details --no-truncate | grep sdb | grep LOGGED_IN -wc ) ] ; then break ; fi
  sleep 1
done
sudo oci-iscsi-config show --details --no-truncate                     >> ${OSlog}
echo "mkdir"                                                           >> ${OSlog}
sudo mkdir /home/opc/bv1                                               >> ${OSlog}
sleep 15
echo "mount"                                                           >> ${OSlog}
sudo mount /dev/oracleoci/oraclevdd1 /home/opc/bv1 && sudo mount | grep /home/opc/bv1
echo "chown"                                                           >> ${OSlog}
sudo chown -R opc:opc /home/opc/bv1                                    >> ${OSlog}
sudo ls /home/opc/bv1                                                  >> ${OSlog}
df -h                                                                  >> ${OSlog}
echo "$(date "+%d.%m.%Y %H:%M:%S") done"                               >> ${OSlog}
sudo chown opc:opc ${BVlog}
cat ${BVlog}
