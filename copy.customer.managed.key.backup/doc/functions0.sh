#!/bin/bash
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
# Update history:
#
# V 1.0.0 15.08.2023 initial version
#

if [ 1 -eq 1 ] ; then # define colors
Color_Off='\e[0m'       # Text Reset

# Regular Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Underline
UBlack='\e[4;30m'       # Black
URed='\e[4;31m'         # Red
UGreen='\e[4;32m'       # Green
UYellow='\e[4;33m'      # Yellow
UBlue='\e[4;34m'        # Blue
UPurple='\e[4;35m'      # Purple
UCyan='\e[4;36m'        # Cyan
UWhite='\e[4;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

# High Intensity
IBlack='\e[0;90m'       # Black
IRed='\e[0;91m'         # Red
IGreen='\e[0;92m'       # Green
IYellow='\e[0;93m'      # Yellow
IBlue='\e[0;94m'        # Blue
IPurple='\e[0;95m'      # Purple
ICyan='\e[0;96m'        # Cyan
IWhite='\e[0;97m'       # White

# Bold High Intensity
BIBlack='\e[1;90m'      # Black
BIRed='\e[1;91m'        # Red
BIGreen='\e[1;92m'      # Green
BIYellow='\e[1;93m'     # Yellow
BIBlue='\e[1;94m'       # Blue
BIPurple='\e[1;95m'     # Purple
BICyan='\e[1;96m'       # Cyan
BIWhite='\e[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\e[0;100m'   # Black
On_IRed='\e[0;101m'     # Red
On_IGreen='\e[0;102m'   # Green
On_IYellow='\e[0;103m'  # Yellow
On_IBlue='\e[0;104m'    # Blue
On_IPurple='\e[0;105m'  # Purple
On_ICyan='\e[0;106m'    # Cyan
On_IWhite='\e[0;107m'   # White
fi

if [ 1 -eq 1 ] ; then # set environement
  . /home/opc/showcase.bv.1/parameter.sh
fi

if [ 1 -eq 1 ] ; then # level 0 functions (this functions do not need/call other then level 0 functions)

function color_print() {
if [ ${DEBUG_LEVEL} -eq 0 ] ; then echo -e "$1: $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -eq 1 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -eq 2 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -gt 2 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
}

TIMESTAMP=$(date "+%Y%m%d%H%M%S")			# timestamp
UNIQUE_ID="k4JgHrt${TIMESTAMP}"				# generate a unique ID to tag resources created by this script
if [ -e /dev/urandom ];then
 UNIQUE_ID=$(cat /dev/urandom|LC_CTYPE=C tr -dc "[:alnum:]"|fold -w 32|head -n 1)
fi
TMP_FILE_LIST=()							# array keeps a list of temporary files to cleanup
THIS_SCRIPT="$(basename ${BASH_SOURCE})"	# sciptname

# Do cleanup
function Cleanup() {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
  for i in "${!TMP_FILE_LIST[@]}"; do
    if [ -f "${TMP_FILE_LIST[$i]}" ]; then
      if [ ${DEBUG_LEVEL} -gt 0 ] ; then  echo -e "deleting ${TMP_FILE_LIST[$i]}" ; fi
      rm -f "${TMP_FILE_LIST[$i]}"
    fi
  done
}

# Do cleanup, display error message and exit
function Interrupt() {
  Cleanup
  exitcode=99
  echo -e "\nScript '${THIS_SCRIPT}' aborted by user. $Color_Off"
  exit $exitcode
}

# trap ctrl-c and call interrupt()
trap Interrupt INT
# trap exit and call cleanup()
trap Cleanup   EXIT

function tempfile()
{
  local __resultvar=$1
  local __tmp_file=$(mktemp -t ${THIS_SCRIPT}_tmp_file.XXXXXXXXXXX) || {
    echo -e "$Cyan ......#*** Creation of ${__tmp_file} failed $Color_Off";
    exit 1;
  }
  TMP_FILE_LIST+=("${__tmp_file}")
  if [[ "$__resultvar" ]]; then
    eval $__resultvar="'$__tmp_file'"
  else
    echo -e "$Cyan ......#$__tmp_file"
  fi
}

function DisplayHelp() {
if [ -z ${1} ] ; then # first parameter is empty 
  color_print "${MYcolor}" "ERROR: need script name" 
else
  head -n 100 ${1} | grep '#@ ' | sed 's/#@  //g'
fi
}

fi

if [ 1 -eq 1 ] ; then # level 1 functions (this functions do need/call level 0 and/or level 1 functions)

function get_instance_IP () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get instance IP in ${REGION_A_PROFILE}" 
  REGION_A_INSTANCE_IP=$( oci --profile "${REGION_A_PROFILE}" --raw-output --query 'data[0]."public-ip"' compute instance list-vnics --instance-id "${REGION_A_INSTANCE_OCID}" )
  color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance IP in ${REGION_A_PROFILE} found: $REGION_A_INSTANCE_IP"
else
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get instance IP in ${REGION_B_PROFILE}" 
  REGION_B_INSTANCE_IP=$( oci --profile "${REGION_B_PROFILE}" --raw-output --query 'data[0]."public-ip"' compute instance list-vnics --instance-id "${REGION_B_INSTANCE_OCID}" )
  color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance IP in ${REGION_B_PROFILE} found: $REGION_B_INSTANCE_IP"
fi
}

function get_instance_OCID () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get instance OCID in ${REGION_A_PROFILE}" 
  tempfile myTEMPFILE
  oci --profile "${REGION_A_PROFILE}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="RUNNING")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_A_PROFILE} instance OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_A_PROFILE} instance Name: $myNAME"
    fi
    if [ "${myNAME}" == "${REGION_A_INSTANCE_NAME}" ] ; then 
      REGION_A_INSTANCE_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance in ${REGION_A_PROFILE} found: $myNAME"
    fi
  done
else
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get instance OCID in ${REGION_B_PROFILE}" 
  tempfile myTEMPFILE
  oci --profile "${REGION_B_PROFILE}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="RUNNING")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_B_PROFILE} instance OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_B_PROFILE} instance Name: $myNAME"
    fi
    if [ "${myNAME}" == "${REGION_B_INSTANCE_NAME}" ] ; then 
      REGION_B_INSTANCE_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance in ${REGION_B_PROFILE} found: $myNAME"
    fi
  done
fi
}

function get_block_volume_OCID () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get block volume OCID in ${REGION_A_PROFILE}" 
  tempfile myTEMPFILE
  oci --profile "${REGION_A_PROFILE}" bv volume list --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${REGION_A_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: region ${REGION_A_PROFILE} block volume OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: region ${REGION_A_PROFILE} block volume Name: $myNAME"
    fi
    if [ 0 -lt $(echo "${myNAME}" | grep "${REGION_A_BLOCK_VOLUME_NAME}" | wc -l)  ] ; then 
      REGION_A_BLOCK_VOLUME_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume in ${REGION_A_PROFILE} found: $myNAME"
    fi
  done
else
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get block volume OCID in ${REGION_B_PROFILE}" 
  tempfile myTEMPFILE
  oci --profile "${REGION_B_PROFILE}" bv volume list --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${REGION_B_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: region ${REGION_B_PROFILE} block volume OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: region ${REGION_B_PROFILE} block volume Name: $myNAME"
    fi
    if [ 0 -lt $(echo "${myNAME}" | grep "${REGION_A_BLOCK_VOLUME_NAME}" | wc -l)  ] ; then 
      REGION_B_BLOCK_VOLUME_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume in ${REGION_B_PROFILE} found: $myNAME"
    fi
  done
fi
}

function get_block_volume_backup_OCID () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'


if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get block volume backup OCID in ${REGION_A_PROFILE}" 
  tempfile myTEMPFILE
  oci --profile "${REGION_A_PROFILE}" bv backup list --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_A_PROFILE} block volume backup OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_A_PROFILE} block volume backup Name: $myNAME"
    fi
    if [ 0 -lt $(echo "${myNAME}" | grep "Backup-${REGION_A_BLOCK_VOLUME_NAME}" | wc -l)  ] ; then 
      REGION_A_BLOCK_VOLUME_BACKUP_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume backup in ${REGION_A_PROFILE} found: $myNAME"
    fi
  done
fi
if [ ${1} = "B" ] ; then # first parameter is B
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get block volume backup OCID in ${REGION_B_PROFILE}" 
  tempfile myTEMPFILE
  oci --profile "${REGION_B_PROFILE}" bv backup list --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_B_PROFILE} block volume backup OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_B_PROFILE} block volume backup Name: $myNAME"
    fi
    if [ 0 -lt $(echo "${myNAME}" | grep "Backup-${REGION_A_BLOCK_VOLUME_NAME}" | wc -l)  ] ; then 
      REGION_B_BLOCK_VOLUME_BACKUP_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume backup in ${REGION_B_PROFILE} found: $myNAME"
    fi
  done
fi

}

function copy_block_volume_backup () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
if [ ${1} = "A" ] && [ ${2} = "B" ] ; then # first parameter is A and second is B
  color_print "${MYcolor}" "${PF1} copy block volume backup from ${REGION_A_PROFILE} to ${REGION_B_PROFILE}"
  oci --profile "${REGION_A_PROFILE}" bv backup copy --volume-backup-id "${REGION_A_BLOCK_VOLUME_BACKUP_OCID}" \
    --destination-region "${REGION_B_IDENTIFIER}" \
    --kms-key-id         "${REGION_B_EncryptionKey_OCID}" \
    --display-name       "Backup-${REGION_A_BLOCK_VOLUME_NAME}" \
    --wait-for-state     "AVAILABLE" 
  REGION_B_BLOCK_VOLUME_BACKUP_NAME="Backup-${REGION_A_BLOCK_VOLUME_NAME}"
else
  color_print "${MYcolor}" "Block Volume backup copy only allowed from Region A to Region B" 
fi
}

function create_instance () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

tempfile myTEMPFILE1
cat <<EOF > ${myTEMPFILE1}
{
  "areAllPluginsDisabled": false,
  "isManagementDisabled": false,
  "isMonitoringDisabled": false,
  "pluginsConfig": [
    {
      "desiredState": "ENABLED",
      "name": "Block Volume Management"
    }
  ]
}
EOF

tempfile myTEMPFILE2
cat <<EOF > ${myTEMPFILE2}
#!/bin/bash
# Version: @(#).new-instance-init.sh
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
echo "This is done by new-instance-init.sh during instance creation time $(date "+%d.%m.%Y %H:%M:%S")" > /home/opc/new-instance-init.log
#echo "$(date "+%d.%m.%Y %H:%M:%S"): starting yum update"         >> /home/opc/new-instance-init.log
#sudo yum -y update                                              >> /home/opc/new-instance-init.log
echo "$(date "+%d.%m.%Y %H:%M:%S"): end of new-instance-init.sh" >> /home/opc/new-instance-init.log
EOF

tempfile myTEMPFILE3A
cat <<EOF > ${myTEMPFILE3A}
{
   "sourceType": "image",
   "imageId": "${REGION_A_IMAGE_OCID}",
   "kmsKeyId": "${REGION_A_EncryptionKey_OCID}"
}
EOF

tempfile myTEMPFILE3B
cat <<EOF > ${myTEMPFILE3B}
{
   "sourceType": "image",
   "imageId": "${REGION_B_IMAGE_OCID}",
   "kmsKeyId": "${REGION_B_EncryptionKey_OCID}"
}
EOF

 
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "$PF2 $FUNCNAME: create instance in ${REGION_A_PROFILE}" 
  oci --profile "${REGION_A_PROFILE}" compute instance launch    \
    --availability-domain      "${REGION_A_AVAILABILITY_DOMAIN}" \
    --compartment-id           "${COMPARTMENT_OCID}"             \
    --shape                    "${INSTANCE_SHAPE_NAME}"          \
    --shape-config             "${INSTANCE_CONFIG_SHAPE}"        \
    --subnet-id                "${REGION_A_SUBNET_OCID}"         \
    --display-name             "${REGION_A_INSTANCE_NAME}"       \
    --assign-public-ip         "true"                            \
    --agent-config             file://${myTEMPFILE1}             \
    --user-data-file           ${myTEMPFILE2}                    \
    --source-details           file://${myTEMPFILE3A}            \
    --wait-for-state           "RUNNING"                         \
    --ssh-authorized-keys-file "${PUBLIC_SSH_KEY}"
  get_instance_OCID "A"
  color_print "${MYcolor}" "$PF2 $FUNCNAME: attache volume ${REGION_A_BLOCK_VOLUME_NAME} to instance in ${REGION_A_PROFILE}" 
  oci --profile "${REGION_A_PROFILE}" compute volume-attachment attach-iscsi-volume    \
    --instance-id                       "${REGION_A_INSTANCE_OCID}"                    \
    --volume-id                         "${REGION_A_BLOCK_VOLUME_OCID}"                \
    --device                            "/dev/oracleoci/oraclevdd"                     \
    --is-agent-auto-iscsi-login-enabled "TRUE"                                         \
    --wait-for-state                    "ATTACHED"
  sleep 20
  color_print "${MYcolor}" "$PF2 $FUNCNAME: run remote script on new instance ${REGION_A_INSTANCE_NAME} in region ${REGION_A_PROFILE}" 
  get_instance_IP "A"
  sleep 30
  RunScriptRegionAsource=${SHOWCASE_DIR}/run.remote.region.A.sh
  RunScriptRegionAtarget=/home/opc/run.remote.region.A.sh
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_A_INSTANCE_IP} ls
  scp ${RunScriptRegionAsource} opc@${REGION_A_INSTANCE_IP}:${RunScriptRegionAtarget}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_A_INSTANCE_IP} sudo chown opc:opc ${RunScriptRegionAtarget}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_A_INSTANCE_IP} sudo chmod 744     ${RunScriptRegionAtarget}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_A_INSTANCE_IP} ${RunScriptRegionAtarget}
  color_print "${MYcolor}" "$PF2 $FUNCNAME: cat /home/opc/bv1/new-bv.log on new instance ${REGION_A_INSTANCE_NAME} in region ${REGION_A_PROFILE}" 
  echo "${LogPF2}"                                                                                                          >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                          >> "${LOG_FILE}"
  echo "${LogPF2} execute cat /home/opc/bv1/new-bv.log on instance ${REGION_A_INSTANCE_NAME} in ${REGION_A_PROFILE}"        >> "${LOG_FILE}"
  echo "${LogPF2}"                                                                                                          >> "${LOG_FILE}"
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_A_INSTANCE_IP} sudo cat /home/opc/bv1/new-bv.log >> "${LOG_FILE}"
  echo "${LogPF2}"                                                                                                          >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                          >> "${LOG_FILE}"
  echo "${LogPF2} execute sudo umount /home/opc/bv1 on instance ${REGION_A_INSTANCE_NAME} in ${REGION_A_PROFILE} te get storage consistency." >> "${LOG_FILE}"
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_A_INSTANCE_IP} sudo umount /home/opc/bv1
  echo "${LogPF1}"                                                                                                          >> "${LOG_FILE}"
else
  color_print "${MYcolor}" "$PF2 $FUNCNAME: create block volume in ${REGION_B_PROFILE}" 
  create_block_volume "B"
  color_print "${MYcolor}" "$PF2 $FUNCNAME: create instance in ${REGION_B_PROFILE}" 
  oci --profile "${REGION_B_PROFILE}" compute instance launch    \
    --availability-domain      "${REGION_B_AVAILABILITY_DOMAIN}" \
    --compartment-id           "${COMPARTMENT_OCID}"             \
    --shape                    "${INSTANCE_SHAPE_NAME}"          \
    --shape-config             "${INSTANCE_CONFIG_SHAPE}"        \
    --subnet-id                "${REGION_B_SUBNET_OCID}"         \
    --display-name             "${REGION_B_INSTANCE_NAME}"       \
    --assign-public-ip         "true"                            \
    --agent-config             file://${myTEMPFILE1}             \
    --user-data-file           ${myTEMPFILE2}                    \
    --source-details           file://${myTEMPFILE3B}            \
    --wait-for-state           "RUNNING"                         \
    --ssh-authorized-keys-file "${PUBLIC_SSH_KEY}"
  get_instance_OCID "B"
  color_print "${MYcolor}" "$PF2 $FUNCNAME: attache volume ${REGION_A_BLOCK_VOLUME_NAME} to instance in ${REGION_B_PROFILE}" 
  oci --profile "${REGION_B_PROFILE}" compute volume-attachment attach-iscsi-volume    \
    --instance-id                       "${REGION_B_INSTANCE_OCID}"                    \
    --volume-id                         "${REGION_B_BLOCK_VOLUME_OCID}"                \
    --device                            "/dev/oracleoci/oraclevdd"                     \
    --is-agent-auto-iscsi-login-enabled "TRUE"                                         \
    --wait-for-state                    "ATTACHED"
  sleep 20
  color_print "${MYcolor}" "$PF2 $FUNCNAME: run remote script on new instance ${REGION_B_INSTANCE_NAME} in region ${REGION_B_PROFILE}" 
  get_instance_IP "B"
  sleep 30
  RunScriptRegionBsource=${SHOWCASE_DIR}/run.remote.region.B.sh
  RunScriptRegionBtarget=/home/opc/run.remote.region.B.sh
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_B_INSTANCE_IP} ls
  scp ${RunScriptRegionBsource} opc@${REGION_B_INSTANCE_IP}:${RunScriptRegionBtarget}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_B_INSTANCE_IP} sudo chown opc:opc ${RunScriptRegionBtarget}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_B_INSTANCE_IP} sudo chmod 744     ${RunScriptRegionBtarget}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_B_INSTANCE_IP} ${RunScriptRegionBtarget}
  color_print "${MYcolor}" "$PF2 $FUNCNAME: cat /home/opc/bv1/new-bv.log on new instance ${REGION_B_INSTANCE_NAME} in region ${REGION_B_PROFILE}" 
  echo "${LogPF2}"                                                                                                          >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                          >> "${LOG_FILE}"
  echo "${LogPF2} execute cat /home/opc/bv1/new-bv.log on instance ${REGION_B_INSTANCE_NAME} in ${REGION_B_PROFILE}"        >> "${LOG_FILE}"
  echo "${LogPF2}"                                                                                                          >> "${LOG_FILE}"
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${REGION_B_INSTANCE_IP} sudo cat /home/opc/bv1/new-bv.log >> "${LOG_FILE}"
  echo "${LogPF2}"                                                                                                          >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                          >> "${LOG_FILE}"
fi
}

function create_block_volume () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "${PF1} create block volume in ${REGION_A_PROFILE}"
  oci --profile "${REGION_A_PROFILE}" bv volume create      \
    --availability-domain "${REGION_A_AVAILABILITY_DOMAIN}" \
    --compartment-id      "${COMPARTMENT_OCID}"             \
    --display-name        "${REGION_A_BLOCK_VOLUME_NAME}"   \
    --size-in-gbs         50                                \
	--kms-key-id          "${REGION_A_EncryptionKey_OCID}"  \
    --wait-for-state      "AVAILABLE" 
  get_block_volume_OCID "A"
else
  get_block_volume_backup_OCID "B"
  color_print "${MYcolor}" "${PF1} create block volume in ${REGION_B_PROFILE} based on a given backup"
  oci --profile "${REGION_B_PROFILE}" bv volume create           \
    --availability-domain "${REGION_B_AVAILABILITY_DOMAIN}"      \
    --compartment-id      "${COMPARTMENT_OCID}"                  \
    --volume-backup-id    "${REGION_B_BLOCK_VOLUME_BACKUP_OCID}" \
	--kms-key-id          "${REGION_B_EncryptionKey_OCID}"       \
    --display-name        "${REGION_A_BLOCK_VOLUME_NAME}"        \
    --wait-for-state      "AVAILABLE" 
  get_block_volume_OCID "B"
fi
}

function create_block_volume_backup () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "${PF1} create block volume backup in ${REGION_A_PROFILE}"
  oci --profile "${REGION_A_PROFILE}" bv backup create --volume-id "${REGION_A_BLOCK_VOLUME_OCID}" \
    --display-name   "Backup-${REGION_A_BLOCK_VOLUME_NAME}" \
    --wait-for-state "AVAILABLE" 
  REGION_A_BLOCK_VOLUME_BACKUP_NAME="Backup-${REGION_A_BLOCK_VOLUME_NAME}"
else
  color_print "${MYcolor}" "Region not A (B not yet implemented)" 
fi
}

function delete_instance () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
 
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  get_instance_OCID "A"
  if [ -z ${REGION_A_INSTANCE_OCID} ]
  then
    color_print "${MYcolor}" "$PF2 $FUNCNAME: no instance in ${REGION_A_PROFILE} to delete" 
  else
    color_print "${MYcolor}" "$PF2 $FUNCNAME: delete instance in ${REGION_A_PROFILE}" 
    oci --profile "${REGION_A_PROFILE}" compute instance terminate     \
    --instance-id                       "${REGION_A_INSTANCE_OCID}"  \
    --wait-for-state                    "TERMINATED"                 \
    --force
  fi
else
  get_instance_OCID "B"
  if [ -z ${REGION_B_INSTANCE_OCID} ]
  then
    color_print "${MYcolor}" "$PF2 $FUNCNAME: no instance in ${REGION_B_PROFILE} to delete" 
  else
    color_print "${MYcolor}" "$PF2 $FUNCNAME: delete instance in ${REGION_B_PROFILE}" 
    oci --profile "${REGION_B_PROFILE}" compute instance terminate     \
    --instance-id                       "${REGION_B_INSTANCE_OCID}"  \
    --wait-for-state                    "TERMINATED"                 \
    --force
  fi
fi
}

function delete_block_volume () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
 
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  get_block_volume_OCID "A"
  if [ -z ${REGION_A_BLOCK_VOLUME_OCID} ]
  then
    color_print "${MYcolor}" "$PF2 $FUNCNAME: no block volume in ${REGION_A_PROFILE} to delete" 
  else
    color_print "${MYcolor}" "$PF2 $FUNCNAME: delete block volume in ${REGION_A_PROFILE}" 
    oci --profile "${REGION_A_PROFILE}" bv volume delete                   \
    --volume-id                         "${REGION_A_BLOCK_VOLUME_OCID}"  \
    --wait-for-state                    "TERMINATED"                     \
    --force
  fi
else
  get_block_volume_OCID "B"
  if [ -z ${REGION_B_BLOCK_VOLUME_OCID} ]
  then
    color_print "${MYcolor}" "$PF2 $FUNCNAME: no block volume in ${REGION_B_PROFILE} to delete" 
  else
    color_print "${MYcolor}" "$PF2 $FUNCNAME: delete block volume in ${REGION_B_PROFILE}" 
    oci --profile "${REGION_B_PROFILE}" bv volume delete                   \
    --volume-id                         "${REGION_B_BLOCK_VOLUME_OCID}"  \
    --wait-for-state                    "TERMINATED"                     \
    --force
  fi
fi
}

function delete_block_volume_backup () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
 
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  get_block_volume_backup_OCID "A"
  if [ -z ${REGION_A_BLOCK_VOLUME_BACKUP_OCID} ]
  then
    color_print "${MYcolor}" "$PF2 $FUNCNAME: no block volume backup in ${REGION_A_PROFILE} to delete" 
  else
    color_print "${MYcolor}" "$PF2 $FUNCNAME: delete block volume backup in ${REGION_A_PROFILE}" 
    oci --profile "${REGION_A_PROFILE}" bv backup delete                          \
    --volume-backup-id                  "${REGION_A_BLOCK_VOLUME_BACKUP_OCID}"  \
    --wait-for-state                    "TERMINATED"                            \
    --force
  fi
else
  get_block_volume_backup_OCID "B"
  if [ -z ${REGION_B_BLOCK_VOLUME_BACKUP_OCID} ]
  then
    color_print "${MYcolor}" "$PF2 $FUNCNAME: no block volume backup in ${REGION_B_PROFILE} to delete" 
  else
    color_print "${MYcolor}" "$PF2 $FUNCNAME: delete block volume backup in ${REGION_B_PROFILE}" 
    oci --profile "${REGION_B_PROFILE}" bv backup delete                          \
    --volume-backup-id                  "${REGION_B_BLOCK_VOLUME_BACKUP_OCID}"  \
    --wait-for-state                    "TERMINATED"                            \
    --force
  fi
fi
}

fi

# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------
