#!/bin/bash
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
# Update history:
#
# V 1.0.0 15.08.2023 initial version
#

# ---------------------------------------------------------------------------------------------------------------------------------------------
# CUSTOMER SPECIFIC VALUES - please update appropriate
# ---------------------------------------------------------------------------------------------------------------------------------------------
if [ 1 -eq 1 ] ; then # global values
export SHOWCASE_DIR=<your_showcase_directory>
export LOG_FILE=<your_log_file_name_including_the_path>                                                 # 
export COMPARTMENT_OCID=<your_compartment_ocid>                                                         # Compartment Name: 
export INSTANCE_SHAPE_NAME=VM.Standard.E4.Flex
export INSTANCE_CONFIG_SHAPE='{ "memoryInGBs": 16, "ocpus": 1 }'
export PUBLIC_SSH_KEY=<your_public_key>
export AD=<your_ad_number>                                                                              # choose your Availability Domain Number
export AD_PREFIX=<your_ad_prefix>                                                                       # Prifix of Availability Domain
fi

if [ 1 -eq 1 ] ; then # Region A values
export REGION_A_PROFILE=<your_region_A>                                                                 # oci (source) region profile e.g. frankfurt
export REGION_A_IDENTIFIER=<your_region_A_identifier>                                                   # find Region Identifier here https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export REGION_A_SUBNET_OCID=<your_region_A_subnet_ocid>                                                 # Subnet OCID
export REGION_A_INSTANCE_NAME="MyInstance-${REGION_A_IDENTIFIER}"                                       # choose your Region A Instance Name
export REGION_A_IMAGE_OCID=<your_region_A_image_ocid>                                                   # All Oracle Linux 9.x Images https://docs.oracle.com/en-us/iaas/images/oracle-linux-9x/
export REGION_A_BLOCK_VOLUME_NAME="BlockVolume-${REGION_A_IDENTIFIER}"                                  # choose your Region A Block Volume Name
export REGION_A_AVAILABILITY_DOMAIN="${AD_PREFIX}:${REGION_A_IDENTIFIER}-AD-${AD}"                      # [AD Prefix]:[Region Identifier]-AD-[Number of Availability Domain] see https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export REGION_A_VAULT_OCID=<your_region_A_vault_ocid>                                                   # Region A Vault OCID
export REGION_A_EncryptionKey_OCID=<your_region_A_erencryptionkey_ocid>                                 # Region A EncryptionKey OCID
fi

if [ 1 -eq 1 ] ; then # Region B values
export REGION_B_PROFILE=<your_region_B>                                                                 # oci (target) region profile e.g. london
export REGION_B_IDENTIFIER=<your_region_B_identifier>                                                   # find Region Identifier here https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export REGION_B_SUBNET_OCID=<your_region_B_subnet_ocid>                                                 # Subnet OCID
export REGION_B_INSTANCE_NAME="MyInstance-${REGION_B_IDENTIFIER}"                                       # choose your Region B Instance Name
export REGION_B_IMAGE_OCID=<your_region_B_image_ocid>                                                   # All Oracle Linux 9.x Images https://docs.oracle.com/en-us/iaas/images/oracle-linux-9x/
export REGION_B_BLOCK_VOLUME_NAME="BlockVolume-${REGION_B_IDENTIFIER}"                                  # choose your Region B Block Volume Name
export REGION_B_AVAILABILITY_DOMAIN="${AD_PREFIX}:${REGION_B_IDENTIFIER}-AD-${AD}"                      # [AD Prefix]:[Region Identifier]-AD-[Number of Availability Domain] see https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export REGION_B_VAULT_OCID=<your_region_B_vault_ocid>                                                   # Region B Vault OCID
export REGION_B_EncryptionKey_OCID=<your_region_B_erencryptionkey_ocid>                                 # Region B EncryptionKey OCID
fi

if [ 1 -eq 1 ] ; then # normally do not touch values
export DEBUG_LEVEL=1                                                                                    # 0 (off); 1 (low); 2 (high)
export PF1=###                                                                                          # Prefix 1 - used to introduce function
export PF2="${PF1}.###:"                                                                                # Prefix 2 - used to log informations inside functions
export LogPF1="###------------------------------------------------------------------------------------" # Log Prefix 1 - used to log informationin LOG_FILE
export LogPF2="### "                                                                                    # Log Prefix 2 - used to log informationin LOG_FILE
export MYcolor="${IYellow}"                                                                             # define output color
if [ ${DEBUG_LEVEL} -eq 0 ] ; then export MYcolor=$ICyan   ; fi
if [ ${DEBUG_LEVEL} -eq 1 ] ; then export MYcolor=$IPurple ; fi
if [ ${DEBUG_LEVEL} -eq 2 ] ; then export MYcolor=$IRed    ; fi
fi
# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------
