# Cloud Resilience by default

[back to Next-Generation Cloud][home]

## Table of Contents
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Cloud Resilience by default](#cloud-resilience-by-default)
	- [Table of Contents](#table-of-contents)
	- [Introduction](#introduction)
	- [Secure your Storage](#secure-your-storage)
	- [Set up your Server with Resilience by default Using the Console](#set-up-your-server-with-resilience-by-default-using-the-console)
	- [Set up your Server with Resilience by default Using the CLI](#set-up-your-server-with-resilience-by-default-using-cli)

<!-- /TOC -->

## Introduction
Here we focus on Cloud Resilience by default or "Keep your systems running without investing any effort".
OCI provide plenty of Cloud Resilience functionality - just make use of it. Here we show how to make use of

- [Live Migration](https://docs.oracle.com/en-us/iaas/Content/Compute/References/infrastructure-maintenance.htm#live-migration)
  - During an infrastructure maintenance event, Oracle Cloud Infrastructure live migrates supported VM instances from the physical VM host that needs maintenance to a healthy VM host without disrupting running instances.
- [Reboot Migration](https://docs.oracle.com/en-us/iaas/Content/Compute/References/infrastructure-maintenance.htm#reboot)
  - For instances with a date in the Reboot Maintenance field (available in the Console, CLI, and SDKs), the instance is stopped, migrated to a healthy host, and restarted. After a migration, by default the instance is recovered to the same lifecycle state as before the maintenance event.
- [Block Volume Durability](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/overview.htm#Durability)
  - The Oracle Cloud Infrastructure Block Volume service offer a high level of data durability compared to standard, attached drives. All volumes are automatically replicated for you, helping to protect against data loss. Multiple copies of data are stored redundantly across multiple storage servers with built-in repair mechanisms. For service level objective, the Block Volume service is designed to provide 99.99 percent annual durability for block volumes and boot volumes. However, we recommend that you make regular backups to protect against the failure of an availability domain.
- [Instance scaling](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/bootvolumes.htm#Boot_Volumes)
  -  When you terminate your instance, you can keep the associated boot volume and use it to launch a new instance using a different instance type or shape. See Creating an Instance for steps to launch an instance based on a boot volume. This allows you to switch easily from a bare metal instance to a VM instance and vice versa, or scale up or down the number of cores for an instance.
- [Policy-Based Backups](https://docs.oracle.com/en-us/iaas/Content/Block/Tasks/schedulingvolumebackups.htm)
  - The Oracle Cloud Infrastructure Block Volume service provides you with the capability to perform volume backups and volume group backups automatically on a schedule and retain them based on the selected backup policy.
- [Cross Region Replication](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/volumereplication.htm)
  - The Block Volume service provides you with the capability to perform ongoing automatic asynchronous replication of block volumes, boot volumes, and volume groups to other regions.
- [Volume Groups](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/volumegroups.htm)
  - The Oracle Cloud Infrastructure Block Volume service provides you with the capability to group together multiple volumes in a volume group. A volume group can include both types of volumes, boot volumes, which are the system disks for your compute instances, and block volumes for your data storage. You can use volume groups to create volume group backups and clones that are point-in-time and crash-consistent.

to get a Server incl. Cloud Resilience by default. This means that the Server is able to survive (incl. all your data in a consistent state) several outage scenarios like e.g.

- hardware defect
  - server hardware (make use of Live Migration or Reboot Migration)
  - storage hardware (make use of Block Volume Durability)
- fault domain down (make use of Live Migration or Reboot Migration)
- availability domain down (make use of Instance scaling)
- region down (make use of Policy-Based Backups and Cross Region Replication)

To see this in action, just have a look to my 6 minute video for developers: [Make your Cloud resilient against regional outages](https://www.youtube.com/watch?v=IVqLe_XH_AE)

We’re excited to [announce the general availability of Oracle Cloud Infrastructure (OCI) Full Stack Disaster Recovery](https://blogs.oracle.com/cloud-infrastructure/post/fsdr-launch). This service is the first true disaster recovery-as-a-service (DRaaS) solution for OCI that provides comprehensive disaster recovery management for an entire application stack with a single click.
Full Stack Disaster Recovery is a flexible, highly extensible, and scalable DRaaS solution for cross-regional disaster recovery in OCI. The service is ideal for moving workloads between OCI regions for maintenance, rolling upgrades, blue and green deployments, and any number of other uses beyond disaster recovery.

[Full Stack Disaster Recovery Service](https://oracle.com/cloud/full-stack-disaster-recovery/) has the built-in intelligence needed to automatically create basic disaster recovery plans based solely on the virtual machines, storage volume groups and databases you’ve included in a DRPG. This feature is one of many that dramatically reduce the amount of effort and time spent configuring disaster recovery.

Accelerate your implementation of business continuity with a higher degree of reliability at a lower cost to implement, maintain, and use.

## Secure your Storage


In today’s data-driven world, securing your information is paramount. My blog 
[OCI helps you to optimize your data protection](https://blogs.oracle.com/cloud-infrastructure/post/oci-helps-optimize-data-protection)
 explores how Oracle Cloud Infrastructure (OCI) helps enterprises protect their data using the 
[CIA triad](https://www.nccoe.nist.gov/publication/1800-26/VolA/index.html): Confidentiality, integrity, and availability.

Organizations benefit from incorporating risk management into their strategic planning, and decision-making at all levels. 
Effective risk management helps to anticipate and address future circumstances proactively, adapting as needed to support the organization’s objectives. 

See also [Risk Management Essentials](https://blogs.oracle.com/security/post/risk-management-essentials) and [Prioritize Business-Critical Risks](https://blogs.oracle.com/security/post/businesscritical-risks)


As a prerequisite, we must replicate the data encryption key to the target region. 
For details on how to replicate your vault, including your master and data encryption keys, see the 
[tutorial on GitHub](https://gitlab.com/hmielimo/cloud-resilience-by-default/-/tree/main/copy.customer.managed.key.backup).
 This replication gives you the OCID of the target data encryption key in the target region, London.

This GitHub tutorial here, introduces an automated crossregion replication of block volumes. Now, we only have to add the 
OCID of the target data encryption key to 
[set up a server with resilience by default](https://gitlab.com/hmielimo/cloud-resilience-by-default/#set-up-your-server-with-resilience-by-default-using-the-console)
, using the Oracle Cloud Console. To automate this process using the OCI CLI, see 
[Set up your Server with Resilience by default using CLI](https://gitlab.com/hmielimo/cloud-resilience-by-default/#set-up-your-server-with-resilience-by-default-using-cli).

This process enables you to automate your production environment and improve confidentiality, integrity, and availability 
for all your servers. You can observe all technical details how OCI helps you to optimize your data protection.


## Set up your Server with Resilience by default Using the Console

Let me show you how easy you can create this Server with Resilience by default.

| action                                               | screenshot                                                                                                                |
| ---------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| Create compute instance (all default)                | <img alt="Create compute instance" src="doc/images/create.compute.instance.jpg" title="Create compute instance" width="100%"> |
| Create Backup Policy myBackupPolicy (optional)       | <img alt="Create Backup Policy" src="doc/images/create.backup.policy.jpg" title="Create Backup Policy" width="100%"> |
| Create block volume                                  | <img alt="Create block volume" src="doc/images/create.block.volume.jpg" title="Create compute instance" width="100%"> |
| Create volume group                                  | <img alt="Create volume group" src="doc/images/create.volume.group.1.jpg" title="Create compute instance" width="100%"> |
| - put boot volume into the volume group              | <img alt="put boot volume into the volume group" src="doc/images/create.volume.group.2.jpg" title="Create compute instance" width="100%"> |
| - put block volume into the volume group             | <img alt="put block volume into the volume group" src="doc/images/create.volume.group.2.jpg" title="Create compute instance" width="100%"> |
| - enable Cross Region Replication                    | <img alt="enable Cross Region Replication" src="doc/images/create.volume.group.3.jpg" title="Create compute instance" width="100%"> |
| - select Backup Policy myBackupPolicy (optional)     | <img alt="select Backup Policy" src="doc/images/add.backup.policy.jpg" title="Add Backup Policy" width="100%"> |
| now asyncron replication is ongoing |  |
| - activate Volume Group Replica in the target region | <img alt="activate Volume Group Replica" src="doc/images/create.volume.group.4.jpg" title="Create compute instance" width="100%"> |

## Set up your Server with Resilience by default using CLI

In production environments it is useful to automate this process (see this in action [Make your Cloud resilient against regional outages](https://www.youtube.com/watch?v=IVqLe_XH_AE)).
Here we give you an example how to do so.

### Description

We have automated all needed manual steps for you.

- Set up production environment (in region Frankfurt)
  - Create compute instance
  - Create block volume
  - Create volume group
  - Enable Cross Region Replication
- Activate Backup environment (in region London)
  - Activate Volume Group Replica in the target region
- Cleanup

The Demonstration gives you two opportunities (after production environment is ready and before activate Backup environment)
to validate details in the logfiles and the real environment.

Please use the code as an example (this is only one way to do it, there are many more) to familiarize yourself with OCIs automation capabilities.

[<img alt="Oracle Cloud Cross Region Replication - Architecture Overview" src="doc/images/Cross.Region.Replication.jpg" title="Oracle Cloud Cross Region Replication - Architecture Overview" width="80%">](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/volumereplication.htm)

### Prerequisite

Please have the following resources ready before starting the script:

- OCI user
- OCI compartment
- your own Vault encryption key OCID in Germany Central (Frankfurt) and UK South (London)
- OCI region access to Germany Central (Frankfurt) and UK South (London)
- OCI VCN and public subnet in region Germany Central (Frankfurt)
- OCI VCN and public subnet in region UK South (London)
- [Setup an operate server](https://gitlab.com/hmielimo/next-generation-cloud/-/tree/main/doc/how.to.create.a.new.operate.server?ref_type=heads)
- copy the software ([parameter.sh](doc/parameter.sh), [functions0.sh](doc/functions0.sh) and [demonstrate.server.resilience.by.default.sh](doc/demonstrate.server.resilience.by.default.sh)) to the operate server
- update parameter.sh with your values

Please be aware that the script creates and terminates OCI resources based on the user permissions only within the  compartment.

### Demonstration

| action               | description                                                  |
| -------------------- | ------------------------------------------------------------ |
| Prerequisite         | please take care of all needed prerequisites |
| start the script     | use opc user on the operate server; be in the home directory: start the script (./demonstrate.server.resilience.by.default.sh) |
| script action 1      | Show environment details (see parameter.sh LOG_FILE="/home/opc/environment.log" )  |
| script action 2      | Cleanup environment (takes about 3 minutes)   |
| script action 3      | Show environment details (see parameter.sh LOG_FILE="/home/opc/environment.log" )   |
| script action 4      | Create environment (instance, block volume, volume group, configure cross region replication) (takes about 2 minutes)   |
| script action 5      | Show environment details (see parameter.sh LOG_FILE="/home/opc/environment.log" )   |
| script action 6      | Configure new server in FRANKFURT |

- configure new server in FRANKFURT
  - get server IP address from LOG_FILE="/home/opc/environment.log"
  - ssh as opc to ServerFrankfurt
  - only if you like to run the Performance-Based Auto-tune UseCase 
<details><summary>Inhalt aufklappen</summary>
    - Oracle provides a seamless [Block Volume Performance](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm) experience incl. Performance-Based Auto-tune over the complete performance range of 10 up to 120 Volume Performance Units (VPU)
	  - Lower Cost: Volume Performance Units (0), Max IOPS per Volume (3,000), Max MBPS per Volume (480)
	  - Balanced: Volume Performance Units (10), Max IOPS per Volume (25,000), Max MBPS per Volume (480)
	  - Higher Performance: Volume Performance Units (20), Max IOPS per Volume (5,000), Max MBPS per Volume (680)
	  - Ultra High Performance: Volume Performance Units (30-120), Max IOPS per Volume (75,000 - 300,000), Max MBPS per Volume (880 - 2,680)
	  - Please use our [**Block Volume Service performance-based auto-tune - Cost comparison**](https://gitlab.com/hmielimo/cloud-resilience-by-default/-/blob/main/doc/Block.Volume.Service.performance.based.auto.tune.Cost.comparison.xlsx) spreadsheet to calculate your individual business case.
    - Summary or what you are able to observe with the Performance-Based Auto-tune UseCase
		 - [Performance-Based Auto-tune](https://docs.oracle.com/en-us/iaas/releasenotes/services/blockvolume/)
		 - [Block Volume Ultra High Performance](https://docs.oracle.com/en-us/iaas/releasenotes/changes/6cf66516-189d-4ce1-8307-5a8dd6578b3c/)
		 - [You can now automatically connect to iSCSI-attached volumes](https://docs.oracle.com/en-us/iaas/releasenotes/changes/4f1c804a-2045-47c8-a6a2-533839a59e02/)
		 - [Cost Analysis](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/costanalysisoverview.htm)
    - important details
      - Elastic Performance Level (Lower Cost, Balanced, Higher Performance) is always available
      - Elastic Performance Level (Ultra High Performance) require prerequisites, e.g. [Multipath-Enabled iSCSI Attachments](https://docs.oracle.com/en-us/iaas/Content/Block/Tasks/configuringmultipathattachments.htm#iSCSI_Attach)
    - prepare multipath
		 - detach FRANKFURT_BLOCK_VOLUME_NAME
		 - edit FRANKFURT_BLOCK_VOLUME_NAME to UHP
		 - attach FRANKFURT_BLOCK_VOLUME_NAME
		 - check Multipath: YES
	- create partition
	~~~
	DEVICEoPATH=/dev/oracleoci/oraclevdb
	ls -l ${DEVICEoPATH}
	Friendly_Name=$(ls -l ${DEVICEoPATH} | awk '{ print $11 }')
	echo $Friendly_Name
	sudo multipath -ll ${Friendly_Name}
	sudo fdisk ${Friendly_Name}
	# create a new primary partition: n,p,i,w
	sudo reboot
	~~~
	- mount and update /etc/fstab
	 ~~~
	 DEVICEoPATH=/dev/oracleoci/oraclevdb
	 DATAoDIR=/mnt/MyBlockVolume
	 Friendly_Name="$(ls -l ${DEVICEoPATH} | awk '{ print $11 }')-part1"
	 echo $Friendly_Name
	 sudo mkfs -t ext4 ${Friendly_Name}
	 sudo mkdir ${DATAoDIR}
	 sudo umount ${DATAoDIR}
	 sudo mount ${Friendly_Name} ${DATAoDIR}
	 sudo chown -R opc:opc ${DATAoDIR}
	 ls -lah ${DATAoDIR}
	 sudo mount | grep ${DATAoDIR}
	 #
	 sudo blkid | grep  ${Friendly_Name}
	 #
	 sudo vi /etc/fstab
	 e.g. UUID=2b4a3e09-1e94-4aa9-90ae-2cfffd08709f /mnt/MyBlockVolume ext4 defaults,_netdev,noatime 0 2
	 #
	 sudo mount -a
	 sudo mount | grep ${DATAoDIR}
	 ~~~
	- create script validate.throughput.sh
	 ~~~
	 mySCRIPTfile=validate.throughput.sh
	 cat <<'EOF'  > "$mySCRIPTfile"
	 #!/bin/bash
	 DATAoDIR=/mnt/MyBlockVolume
	 MYtestfile=${DATAoDIR}/.test.throughput.tmp
	 myBLOCKlogFILE=${DATAoDIR}/BLOCKlogFILE.log
	 MYlogfile=/tmp/.log.throughput.tmp
	 MYtestamount=1G
	 MYruns=50
	 myMESSAGE="backup ($MYruns times of $MYtestamount) is running:"
	 i=1
	 while [[ $i -le $MYruns ]]
	 do
	   # run dd sync and output throughput
	   MYtestfileTMP=${MYtestfile}.2.$i.$RANDOM
	   dd if=/dev/zero of=${MYtestfileTMP} bs=${MYtestamount} count=1  oflag=dsync 2> ${MYlogfile}
	   MYthroughput=$(cat ${MYlogfile} | grep bytes | awk '{ print $10 }')
	   MYunit=$(cat ${MYlogfile} | grep bytes | awk '{ print $11 }')
	   echo "$(date "+%d.%m.%Y %H:%M:%S") update made from $HOSTNAME - measured throughput of BlockVolumeFrankfurt during $myMESSAGE: ${MYthroughput} ${MYunit}" >> ${myBLOCKlogFILE}
	   rm -f /mnt/MyBlockVolume/.test.throughput.tmp*
	   rm -f ${MYlogfile}
	   ((i = i + 1))
	 done
	 # ------------- eof -------------------
	 EOF
	 sudo chown opc:opc ${mySCRIPTfile}
	 sudo chmod 777 ${mySCRIPTfile}
	 ~~~
	- run validate.throughput.sh (this will run approx. 10 minutes and simulate a 50 GB backup scenario)
      ~~~
      /home/opc/validate.throughput.sh
      cat /mnt/MyBlockVolume/BLOCKlogFILE.log
      ~~~
	- Cost Analysis
		- goto [Cost Analysis](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/costanalysisoverview.htm)
		- select your date (only 1 day)
		- select Granularity Hourly
		- add Filter: Product Description Block Volume - Performance Units
		- add Filter: [your compartment Name]
		- apply
		- please keep in mind
		 - there is a ramp up and also a ramp down phase. If a volume is being throttled at the current setting, auto-tune gradually ramps up and continues to monitor the volume’s performance in increments of 10 VPU/GB, up to Maximum VPU/GB value that the customer configured for that volume.
		 - Volume Size 50 GB => Max Throughput 90 MB/s; Max IOPS 11,500 see [Volume Size and Performance](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeultrahighperformance.htm#ultrahighperfvolumesizeperformance)

	- Summary or what you observed with the Performance-Based Auto-tune UseCase Auto-tune UseCase
      - [Performance-Based Auto-tune](https://docs.oracle.com/en-us/iaas/releasenotes/services/blockvolume/)
      - [Block Volume Ultra High Performance](https://docs.oracle.com/en-us/iaas/releasenotes/changes/6cf66516-189d-4ce1-8307-5a8dd6578b3c/)
      - [You can now automatically connect to iSCSI-attached volumes](https://docs.oracle.com/en-us/iaas/releasenotes/changes/4f1c804a-2045-47c8-a6a2-533839a59e02/)
      - [Cost Analysis](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/costanalysisoverview.htm)
    - end of the Performance-Based Auto-tune UseCase 
</details>

  - format and mount block volume
    ~~~
    DEVICEoPATH=/dev/oracleoci/oraclevdb
    DATAoDIR=/mnt/MyBlockVolume
    sudo mkdir /mnt/MyBlockVolume
    sudo mkfs -t ext4 ${DEVICEoPATH}
    sudo mount ${DEVICEoPATH} ${DATAoDIR}
    sudo chown -R opc:opc ${DATAoDIR}
    ls -lah ${DATAoDIR}
    sudo mount | grep ${DATAoDIR}
    ~~~
  - update fstab
    ~~~
    sudo vi /etc/fstab
    /dev/oracleoci/oraclevdb  /mnt/MyBlockVolume    ext4    defaults,_netdev,noatime  0  2
    ~~~
  - create script to update a log (on boot volume and block volume) each minute
    ~~~
    mySCRIPTfile=validate.replication.sh
    cat <<'EOF'  > "$mySCRIPTfile"
    #!/bin/bash
    myBOOTlogFILE=/home/opc/BOOTlogFILE.log
    myBLOCKlogFILE=/mnt/MyBlockVolume/BLOCKlogFILE.log
    echo "$(date "+%d.%m.%Y %H:%M:%S") update made from $HOSTNAME" >> ${myBOOTlogFILE}
    echo "$(date "+%d.%m.%Y %H:%M:%S") update made from $HOSTNAME" >> ${myBLOCKlogFILE}
    # ------------- eof -------------------
    EOF
    sudo chown opc:opc ${mySCRIPTfile}
    sudo chmod 777 ${mySCRIPTfile}
    ls -lah ${mySCRIPTfile}
    cat ${mySCRIPTfile}
    /home/opc/validate.replication.sh
    cat /home/opc/BOOTlogFILE.log
    cat /mnt/MyBlockVolume/BLOCKlogFILE.log
    ~~~
  - update crontab with script
    ~~~
    myTMPfile=/tmp/.tempfile123
    (
     echo "# ---     validate.replication.sh to be run every minute     --- #"
     echo "*/1  *  *  *  * /home/opc/validate.replication.sh > /dev/null"
     echo "# --------------- MUST - last line of crontab - MUST --------------------------"
    ) > ${myTMPfile}
    cat ${myTMPfile} | sudo crontab -u opc -
    sudo rm -f ${myTMPfile}
    sudo crontab -u opc -l
    ~~~



| action               | description                                                  |
| -------------------- | ------------------------------------------------------------ |
| script action 7      | Activate Volume Group Replica and create new server in target region |
| script action 9      | Show environment details (see parameter.sh LOG_FILE="/home/opc/environment.log" ) |
| script action 10     | Test new server in LONDON |

- test new server in LONDON
  - get server IP address from LOG_FILE="/home/opc/environment.log"
  - ssh as opc to ServerLondon
  - check mount of block volume
    ~~~
    DATAoDIR=/mnt/MyBlockVolume
    sudo mount | grep ${DATAoDIR}
    ls -lah ${DATAoDIR}
    ~~~
  - check log files on ServerLondon
    ~~~
    tail -f /home/opc/BOOTlogFILE.log
    DATAoDIR=/mnt/MyBlockVolume
    tail -f ${DATAoDIR}/BLOCKlogFILE.log
    ~~~

| action               | description                                                  |
| -------------------- | ------------------------------------------------------------ |
| script action 11     | Show environment details (see parameter.sh LOG_FILE="/home/opc/environment.log" ) |
| script action 12     | Cleanup |

### Summary

Now you are able to automate your production environment and get resilience by default for all your servers. You can observe in detail how your individual [Recovery Point Objective (RPO)](https://en.wikipedia.org/wiki/Disaster_recovery#Recovery_Point_Objective) and [Recovery Time Objective (RTO)](https://en.wikipedia.org/wiki/Disaster_recovery#Recovery_Time_Objective)  add up to your overall recovery time in case of a disaster.



<!--- Links -->

[home]:                        https://gitlab.com/hmielimo/next-generation-cloud

<!-- /Links -->
