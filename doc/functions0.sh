#!/bin/bash
# Author: Heinz Mielimonka
# Version: @(#).function0.sh (c) Heinz Mielimonka
#
#@  Prepare and run Cross Region Replication.
#@
#
# Update history:
#
# V 1.1.0 15.10.2024 add customer managed key
# V 1.0.0 21.07.2022 New version
#

if [ 1 -eq 1 ] ; then # define colors
Color_Off='\e[0m'       # Text Reset

# Regular Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Underline
UBlack='\e[4;30m'       # Black
URed='\e[4;31m'         # Red
UGreen='\e[4;32m'       # Green
UYellow='\e[4;33m'      # Yellow
UBlue='\e[4;34m'        # Blue
UPurple='\e[4;35m'      # Purple
UCyan='\e[4;36m'        # Cyan
UWhite='\e[4;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

# High Intensity
IBlack='\e[0;90m'       # Black
IRed='\e[0;91m'         # Red
IGreen='\e[0;92m'       # Green
IYellow='\e[0;93m'      # Yellow
IBlue='\e[0;94m'        # Blue
IPurple='\e[0;95m'      # Purple
ICyan='\e[0;96m'        # Cyan
IWhite='\e[0;97m'       # White

# Bold High Intensity
BIBlack='\e[1;90m'      # Black
BIRed='\e[1;91m'        # Red
BIGreen='\e[1;92m'      # Green
BIYellow='\e[1;93m'     # Yellow
BIBlue='\e[1;94m'       # Blue
BIPurple='\e[1;95m'     # Purple
BICyan='\e[1;96m'       # Cyan
BIWhite='\e[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\e[0;100m'   # Black
On_IRed='\e[0;101m'     # Red
On_IGreen='\e[0;102m'   # Green
On_IYellow='\e[0;103m'  # Yellow
On_IBlue='\e[0;104m'    # Blue
On_IPurple='\e[0;105m'  # Purple
On_ICyan='\e[0;106m'    # Cyan
On_IWhite='\e[0;107m'   # White
fi

if [ 1 -eq 1 ] ; then # set environement
. /home/opc/Cloud.Resilience.by.default/parameter.sh
fi

if [ 1 -eq 1 ] ; then # level 0 functions (this functions do not need/call other then level 0 functions)

function color_print() {
if [ ${DEBUG_LEVEL} -eq 0 ] ; then echo -e "$1: $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -eq 1 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -eq 2 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -gt 2 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
}

TIMESTAMP=$(date "+%Y%m%d%H%M%S")           # timestamp
UNIQUE_ID="k4JgHrt${TIMESTAMP}"             # generate a unique ID to tag resources created by this script
if [ -e /dev/urandom ];then
 UNIQUE_ID=$(cat /dev/urandom|LC_CTYPE=C tr -dc "[:alnum:]"|fold -w 32|head -n 1)
fi
TMP_FILE_LIST=()                            # array keeps a list of temporary files to cleanup
THIS_SCRIPT="$(basename ${BASH_SOURCE})"    # sciptname

# Do cleanup
function Cleanup() {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
  for i in "${!TMP_FILE_LIST[@]}"; do
    if [ -f "${TMP_FILE_LIST[$i]}" ]; then
      #echo -e "deleting ${TMP_FILE_LIST[$i]}"
      rm -f "${TMP_FILE_LIST[$i]}"
    fi
  done
}

# Do cleanup, display error message and exit
function Interrupt() {
  Cleanup
  exitcode=99
  echo -e "\nScript '${THIS_SCRIPT}' aborted by user. $Color_Off"
  exit $exitcode
}

# trap ctrl-c and call interrupt()
trap Interrupt INT
# trap exit and call cleanup()
trap Cleanup   EXIT

function tempfile()
{
  local __resultvar=$1
  local __tmp_file=$(mktemp -t ${THIS_SCRIPT}_tmp_file.XXXXXXXXXXX) || {
    echo -e "$Cyan ......#*** Creation of ${__tmp_file} failed $Color_Off";
    exit 1;
  }
  TMP_FILE_LIST+=("${__tmp_file}")
  if [[ "$__resultvar" ]]; then
    eval $__resultvar="'$__tmp_file'"
  else
    echo -e "$Cyan ......#$__tmp_file"
  fi
}

fi

if [ 1 -eq 1 ] ; then # level 1 functions (this functions do need/call level 0 and/or level 1 functions)

function show_environment () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
PREFIX="### "
PREFIX2="###.### "
color_print "${MYcolor}" "#################################################################################################################################################"
color_print "${MYcolor}" "${PREFIX}   show environment ($(date "+%d.%m.%Y %H:%M:%S"))"
color_print "${MYcolor}" "#################################################################################################################################################"
echo "${PREFIX}   show environment ($(date "+%d.%m.%Y %H:%M:%S"))" >> "${LOG_FILE}"
echo "#################################################################################################################################################" >> "${LOG_FILE}"

: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" iam tenancy get --tenancy-id "${TENENCY_OCID}" > "${myTEMPFILE}"
TENENCY_NAME=$(cat "${myTEMPFILE}"|jq --raw-output '.data.name')

tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" iam compartment get --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
COMPARTMENT_NAME=$(cat "${myTEMPFILE}"|jq --raw-output '.data.name')

if [ $DEBUG_LEVEL -gt 0 ] ; then 
  color_print "${MYcolor}" "${PREFIX} TENENCY_OCID...............: $TENENCY_OCID"
  color_print "${MYcolor}" "${PREFIX} USER_OCID..................: $USER_OCID"
  color_print "${MYcolor}" "${PREFIX} COMPARTMENT_OCID...........: $COMPARTMENT_OCID"
  color_print "${MYcolor}" "${PREFIX} FINGERPRINT................: $FINGERPRINT"
  color_print "${MYcolor}" "${PREFIX} TENENCY_OCID...............: $TENENCY_OCID"
fi
color_print "${MYcolor}" "${PREFIX} TENENCY_NAME...............: $TENENCY_NAME"
color_print "${MYcolor}" "${PREFIX} COMPARTMENT_NAME...........: $COMPARTMENT_NAME"
echo "${PREFIX} TENENCY_NAME...............: $TENENCY_NAME"     >> "${LOG_FILE}"
echo "${PREFIX} COMPARTMENT_NAME...........: $COMPARTMENT_NAME" >> "${LOG_FILE}"


for MYPROFILE in $( echo "${PROFILE_FRANKFURT}" ; echo "${PROFILE_LONDON}" )
do

color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
color_print "${MYcolor}" "${PREFIX}   show ${MYPROFILE} environment"
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
echo "--------------------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"
echo "${PREFIX} show ${MYPROFILE} environment                                                                                     ${PREFIX}" >> "${LOG_FILE}"
echo "--------------------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"

if [ "find instance" == "find instance" ] ; then
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
color_print "${MYcolor}" "${PREFIX} INSTANCES"
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"
echo "${PREFIX} INSTANCES" >> "${LOG_FILE}"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"

tempfile myTEMPFILE
oci --profile "${MYPROFILE}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  
  if [ "${myNAME}" == "${FRANKFURT_SERVER_NAME}" ] && [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then 
  
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"

  fi
  if [ "${myNAME}" == "${LONDON_SERVER_NAME}" ] && [ "${MYPROFILE}" == "${PROFILE_LONDON}" ] ; then 
  
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"

  fi
done

sleep 10

tempfile myTEMPFILE
oci --profile "${MYPROFILE}" compute instance list-vnics --all --compartment-id  "${COMPARTMENT_OCID}"  > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[].'\"display-name\"' ')
do
  myNAME=$ii
  myIP=$(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"display-name\"'=='\"$ii\"')] | .[].'\"public-ip\"' ' )
  if [ "${myNAME}" == "${FRANKFURT_SERVER_NAME}" ] && [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then 
  
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myIP"
    echo "${PREFIX2} $myNAME, $myIP" >> "${LOG_FILE}"

  fi
  if [ "${myNAME}" == "${LONDON_SERVER_NAME}" ] && [ "${MYPROFILE}" == "${PROFILE_LONDON}" ] ; then 
  
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myIP"
    echo "${PREFIX2} $myNAME, $myIP" >> "${LOG_FILE}"

  fi
done





fi

if [ "find volueme group" == "find volueme group" ] ; then
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
color_print "${MYcolor}" "${PREFIX} VOLUEME GROUP"
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"
echo "${PREFIX} VOLUEME GROUP" >> "${LOG_FILE}"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"

tempfile myTEMPFILE
oci --profile "${MYPROFILE}" bv volume-group list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ "${myNAME}" == "${FRANKFURT_VOLUME_GROUP_NAME}" ] ; then 
  
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"

  fi
  if [ "${myNAME}" == "${LONDON_VOLUME_GROUP_NAME}" ] ; then 
  
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
    
  fi
done


fi

if [ "find block voluemes" == "find block voluemes" ] ; then
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
color_print "${MYcolor}" "${PREFIX} BLOCK VOLUEMES"
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"
echo "${PREFIX} BLOCK VOLUEMES" >> "${LOG_FILE}"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"

tempfile myTEMPFILE
if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then oci --profile "${MYPROFILE}" bv volume list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}" ; fi
if [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] ; then oci --profile "${MYPROFILE}" bv volume list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${LONDON_AVAILABILITY_DOMAIN}"    > "${myTEMPFILE}" ; fi
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] && [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_BLOCK_VOLUME_NAME}" | wc -l) ] ; then 
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
  fi
  if [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] && [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_BLOCK_VOLUME_NAME}" | wc -l) ] ; then 
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
  fi
done


fi

if [ "find block volueme replicas" == "find block volueme replicas" ] ; then
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
color_print "${MYcolor}" "${PREFIX} BLOCK VOLUEME REPLICAS"
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"
echo "${PREFIX} BLOCK VOLUEME REPLICAS" >> "${LOG_FILE}"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"

tempfile myTEMPFILE
if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then oci --profile "${MYPROFILE}" bv block-volume-replica list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}" > "${myTEMPFILE}" ; fi
if [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] ; then oci --profile "${MYPROFILE}" bv block-volume-replica list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${LONDON_AVAILABILITY_DOMAIN}"    > "${myTEMPFILE}" > "${myTEMPFILE}" ; fi
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] && [ "${myNAME}" == "${FRANKFURT_BLOCK_VOLUME_NAME}" ] ; then 
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
  fi
  if [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] && [ "${myNAME}" == "${FRANKFURT_BLOCK_VOLUME_NAME}" ] ; then 
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
  fi
done


fi

if [ "find boot voluemes" == "find boot voluemes" ] ; then
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
color_print "${MYcolor}" "${PREFIX} BOOT VOLUEMES"
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"
echo "${PREFIX} BOOT VOLUEMES" >> "${LOG_FILE}"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"

tempfile myTEMPFILE
if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then oci --profile "${MYPROFILE}" bv boot-volume list --all --compartment-id "${COMPARTMENT_OCID}"  --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}" ; fi
if [ "${MYPROFILE}" == "${PROFILE_LONDON}" ]    ; then oci --profile "${MYPROFILE}" bv boot-volume list --all --compartment-id "${COMPARTMENT_OCID}"  --availability-domain "${LONDON_AVAILABILITY_DOMAIN}"    > "${myTEMPFILE}" ; fi
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] && [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_SERVER_NAME}" | wc -l) ] ; then 
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
  fi
  if [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] && [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_SERVER_NAME}" | wc -l) ] ; then 
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
  fi
done


fi

if [ "find boot volueme replicas" == "find boot volueme replicas" ] ; then
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
color_print "${MYcolor}" "${PREFIX} BOOT VOLUEME REPLICAS"
color_print "${MYcolor}" "----------------------------------------------------------------------------------------------------------"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"
echo "${PREFIX} BOOT VOLUEME REPLICAS" >> "${LOG_FILE}"
echo "----------------------------------------------------------------------------------------------------------" >> "${LOG_FILE}"

tempfile myTEMPFILE
if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then oci --profile "${MYPROFILE}" bv boot-volume-replica list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}" > "${myTEMPFILE}" ; fi
if [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] ; then oci --profile "${MYPROFILE}" bv boot-volume-replica list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${LONDON_AVAILABILITY_DOMAIN}"    > "${myTEMPFILE}" > "${myTEMPFILE}" ; fi
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] && [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_SERVER_NAME}" | wc -l)  ] ; then 
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
  fi
  if [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] && [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_SERVER_NAME}" | wc -l) ] ; then 
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
    echo "${PREFIX2} $myNAME, $myOCID" >> "${LOG_FILE}"
  fi
done


fi

done

}

function delete_environment () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

delete_instance
delete_volume_group
delete_block_volume
delete_boot_volume

}

function delete_instance () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

for MYPROFILE in $( echo "${PROFILE_FRANKFURT}" ; echo "${PROFILE_LONDON}" )
do
  color_print "${MYcolor}" "$PF2 $FUNCNAME: terminate instance in ${MYPROFILE}"
  tempfile myTEMPFILE
  oci --profile "${MYPROFILE}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: instance OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: instance Name: $myNAME"
    fi
    if [ "${myNAME}" == "${FRANKFURT_SERVER_NAME}" ] && [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: DELETE INSTANCE: $myNAME, $myOCID"
      oci --profile "${MYPROFILE}" compute instance terminate --instance-id "${myOCID}" --wait-for-state "TERMINATED" --force
    fi
    if [ "${myNAME}" == "${LONDON_SERVER_NAME}" ]    && [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: DELETE INSTANCE: $myNAME, $myOCID"
      oci --profile "${MYPROFILE}" compute instance terminate --instance-id "${myOCID}" --wait-for-state "TERMINATED" --force
    fi
  done
done

}

function delete_volume_group () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

for MYPROFILE in $( echo "${PROFILE_FRANKFURT}" ; echo "${PROFILE_LONDON}" )
do
  color_print "${MYcolor}" "$PF2 $FUNCNAME: delete volume group in ${MYPROFILE}"
  tempfile myTEMPFILE
  oci --profile "${MYPROFILE}" bv volume-group list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: volume group OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: volume group Name: $myNAME"
    fi
    if [ "${myNAME}" == "${FRANKFURT_VOLUME_GROUP_NAME}" ] && [ "${PROFILE_FRANKFURT}" == "${MYPROFILE}" ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: SET VOLUME GROUP CROSS REGION REPLICATION OFF" 
      oci --profile "${MYPROFILE}" bv volume-group update --volume-group-id "${myOCID}" --volume-group-replicas '[]' --wait-for-state "AVAILABLE"  --force  
      color_print "${MYcolor}" "$PF2 $FUNCNAME: DELETE VOLUME GROUP: $myNAME, $myOCID" 
      oci --profile "${MYPROFILE}" bv volume-group delete --volume-group-id "${myOCID}" --force
    fi  
    if [ "${myNAME}" == "${LONDON_VOLUME_GROUP_NAME}" ] && [ "${PROFILE_LONDON}" == "${MYPROFILE}" ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: SET VOLUME GROUP CROSS REGION REPLICATION OFF" 
      oci --profile "${MYPROFILE}" bv volume-group update --volume-group-id "${myOCID}" --volume-group-replicas '[]' --wait-for-state "AVAILABLE"  --force
      color_print "${MYcolor}" "$PF2 $FUNCNAME: DELETE VOLUME GROUP: $myNAME, $myOCID" 
      oci --profile "${MYPROFILE}" bv volume-group delete --volume-group-id "${myOCID}" --force
    fi
  done
done

}

function delete_block_volume () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

for MYPROFILE in $( echo "${PROFILE_FRANKFURT}" ; echo "${PROFILE_LONDON}" ; echo "${PROFILE_FRANKFURT}" ; echo "${PROFILE_LONDON}")
do
  color_print "${MYcolor}" "$PF2 $FUNCNAME: delete block volume in ${MYPROFILE}"
  tempfile myTEMPFILE
  oci --profile "${MYPROFILE}" bv volume list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume Name: $myNAME"
    fi
    if [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_BLOCK_VOLUME_NAME}" | wc -l)  ] && [ "${PROFILE_FRANKFURT}" == "${MYPROFILE}" ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: SET BLOCK VOLUME CROSS REGION REPLICATION OFF" 
      oci --profile "${MYPROFILE}" bv volume update --volume-id "${myOCID}" --block-volume-replicas '[]' --wait-for-state "AVAILABLE" --force 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: DELETE BLOCK VOLUME: $myNAME, $myOCID" 
      oci --profile "${MYPROFILE}" bv volume delete --volume-id "${myOCID}" --force
    fi  
    if [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_BLOCK_VOLUME_NAME}" | wc -l) ] && [ "${PROFILE_LONDON}" == "${MYPROFILE}" ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: SET BLOCK VOLUME CROSS REGION REPLICATION OFF" 
      oci --profile "${MYPROFILE}" bv volume update --volume-id "${myOCID}" --block-volume-replicas '[]' --wait-for-state "AVAILABLE" --force 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: DELETE BLOCK VOLUME: $myNAME, $myOCID" 
      oci --profile "${MYPROFILE}" bv volume delete --volume-id "${myOCID}" --force
    fi
  done
done

}

function delete_boot_volume () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

for MYPROFILE in $( echo "${PROFILE_FRANKFURT}" ; echo "${PROFILE_LONDON}" ; echo "${PROFILE_FRANKFURT}" ; echo "${PROFILE_LONDON}")
do
  color_print "${MYcolor}" "$PF2 $FUNCNAME: delete boot volume in ${MYPROFILE}"
  tempfile myTEMPFILE
  if [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then oci --profile "${MYPROFILE}" bv boot-volume list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}" ; fi
  if [ "${MYPROFILE}" == "${PROFILE_LONDON}"    ] ; then oci --profile "${MYPROFILE}" bv boot-volume list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${LONDON_AVAILABILITY_DOMAIN}"    > "${myTEMPFILE}" ; fi
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: boot volume OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: boot volume Name: $myNAME"
    fi
    if [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_SERVER_NAME}" | wc -l)  ] && [ "${PROFILE_FRANKFURT}" == "${MYPROFILE}" ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: SET BOOT VOLUME CROSS REGION REPLICATION OFF" 
      oci --profile "${MYPROFILE}" bv boot-volume update --boot-volume-id "${myOCID}" --boot-volume-replicas '[]' --wait-for-state "AVAILABLE" --force 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: DELETE BOOT VOLUME: $myNAME, $myOCID" 
      oci --profile "${MYPROFILE}" bv boot-volume delete --boot-volume-id "${myOCID}" --force
    fi  
    if [ 0 -lt $(echo "${myNAME}" | grep "${FRANKFURT_SERVER_NAME}" | wc -l) ] && [ "${PROFILE_LONDON}" == "${MYPROFILE}" ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: SET BOOT VOLUME CROSS REGION REPLICATION OFF" 
      oci --profile "${MYPROFILE}" bv boot-volume update --boot-volume-id "${myOCID}" --boot-volume-replicas '[]' --wait-for-state "AVAILABLE" --force 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: DELETE BOOT VOLUME: $myNAME, $myOCID" 
      oci --profile "${MYPROFILE}" bv boot-volume delete --boot-volume-id "${myOCID}" --force
    fi
  done
done

}

function create_instance () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

color_print "${MYcolor}" "$PF2 $FUNCNAME: create instance in $PROFILE_FRANKFURT"


tempfile myTEMPFILE
cat <<EOF > ${myTEMPFILE}
{
  "areAllPluginsDisabled": false,
  "isManagementDisabled": false,
  "isMonitoringDisabled": false,
  "pluginsConfig": [
    {
      "desiredState": "ENABLED",
      "name": "Block Volume Management"
    }
  ]
}
EOF

color_print "${MYcolor}" "$PF2 $FUNCNAME: create instance (boot volume) - ORACLE managed key"
oci --profile "${PROFILE_FRANKFURT}" compute instance launch --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" \
 --compartment-id      "${COMPARTMENT_OCID}" \
 --shape               "${INSTANCE_SHAPE_NAME}" \
 --shape-config        "${INSTANCE_CONFIG_SHAPE}" \
 --image-id            "${IMAGE_FRANKFURT_OCID}" \
 --subnet-id           "${FRANKFURT_SUBNET_OCID}" \
 --display-name        "${FRANKFURT_SERVER_NAME}" \
 --assign-public-ip    "true" \
 --agent-config        file://${myTEMPFILE} \
 --wait-for-state      "RUNNING" \
 --ssh-authorized-keys-file "${PUBLIC_SSH_KEY}"

tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="RUNNING")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ $DEBUG_LEVEL -gt 0 ] ; then 
    color_print "${MYcolor}" "$PF2 $FUNCNAME: instance OCID: $myOCID"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: instance Name: $myNAME"
  fi
  if [ "${myNAME}" == "${FRANKFURT_SERVER_NAME}" ] ; then 
  
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance in ${PROFILE_FRANKFURT} found: $myNAME"
    
    if [ "${CUSTOMER_MANAGED_KEY}" == "Y" ] ; then
      tempfile myTEMPFILE
      oci --profile "${PROFILE_FRANKFURT}" compute boot-volume-attachment list \
          --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}"             \
          --compartment-id      "${COMPARTMENT_OCID}"                          \
          --instance-id "${myOCID}" > "${myTEMPFILE}"
      myOCID=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | .[].'\"boot-volume-id\"' ' )
      color_print "${MYcolor}" "$PF2 $FUNCNAME: update instance (boot volume ${myOCID}) - CUSTOMER managed key"
      oci --profile "${PROFILE_FRANKFURT}" bv boot-volume-kms-key update \
          --boot-volume-id "${myOCID}"                                   \
          --kms-key-id "${FRANKFURT_KEY_OCID}" 
    fi 
  fi
done

}

function create_block_volume () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
if [ "${CUSTOMER_MANAGED_KEY}" == "N" ] ; then
color_print "${MYcolor}" "$PF2 $FUNCNAME: create block volume - ORACLE managed key"
oci --profile "${PROFILE_FRANKFURT}" bv volume create --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" \
 --compartment-id "${COMPARTMENT_OCID}" \
 --display-name   "${FRANKFURT_BLOCK_VOLUME_NAME}" \
 --size-in-gbs    50 \
 --wait-for-state "AVAILABLE" 
fi

if [ "${CUSTOMER_MANAGED_KEY}" == "Y" ] ; then
color_print "${MYcolor}" "$PF2 $FUNCNAME: create block volume - CUSTOMER managed key"
oci --profile "${PROFILE_FRANKFURT}" bv volume create --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" \
 --compartment-id "${COMPARTMENT_OCID}" \
 --display-name   "${FRANKFURT_BLOCK_VOLUME_NAME}" \
 --size-in-gbs    50 \
 --kms-key-id   "${FRANKFURT_KEY_OCID}" \
 --wait-for-state "AVAILABLE" 
fi

tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" bv volume list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ $DEBUG_LEVEL -gt 0 ] ; then 
    color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume OCID: $myOCID"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume Name: $myNAME"
  fi
  
  num=$(echo "${myNAME}" | grep "${FRANKFURT_BLOCK_VOLUME_NAME}" | wc -l)
  
  if [ $num -gt 0 ] ; then 
    
    TMP_BLOCK_VOLUME_OCID=$myOCID
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Block Volume in ${PROFILE_FRANKFURT} found: $myNAME"

  fi
  
done


for MYPROFILE in $( echo "${PROFILE_FRANKFURT}" )
do
  tempfile myTEMPFILE
  oci --profile "${MYPROFILE}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ "${myNAME}" == "${FRANKFURT_SERVER_NAME}" ] && [ "${MYPROFILE}" == "${PROFILE_FRANKFURT}" ] ; then 
      TMP_INSTANCE_OCID=$myOCID  
    fi
  done
done

color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume OCID: $TMP_BLOCK_VOLUME_OCID"
color_print "${MYcolor}" "$PF2 $FUNCNAME: instance OCID: $TMP_INSTANCE_OCID"

color_print "${MYcolor}" "$PF2 $FUNCNAME: ATTACH BLOCK VOLUME TO INSTANCE"
oci --profile "${PROFILE_FRANKFURT}" compute volume-attachment attach-iscsi-volume \
 --instance-id                        "$TMP_INSTANCE_OCID" \
 --volume-id                          "$TMP_BLOCK_VOLUME_OCID" \
 --device                             "/dev/oracleoci/oraclevdb" \
 --is-agent-auto-iscsi-login-enabled  "YES" \
 --wait-for-state                     "ATTACHED" 

}

function create_volume_group () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

if [ "find instance boot volume OCID" == "find instance boot volume OCID" ] ; then

sleep 10
tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="RUNNING")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ "${myNAME}" == "${FRANKFURT_SERVER_NAME}" ] ; then 
  
    FANKFURT_INSTANCE_OCID="${myOCID}"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance OCID: $myOCID"

  fi
done

if [ "${FANKFURT_INSTANCE_OCID}" == "" ] ; then
sleep 20
tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="RUNNING")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ "${myNAME}" == "${FRANKFURT_SERVER_NAME}" ] ; then 
  
    FANKFURT_INSTANCE_OCID="${myOCID}"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance OCID: $myOCID"

  fi
done
fi

if [ "${FANKFURT_INSTANCE_OCID}" == "" ] ; then color_print "${MYcolor}" "$PF2 $FUNCNAME: after 30 sec Instance not RUNNING" ; fi


tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" compute boot-volume-attachment list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$FANKFURT_INSTANCE_OCID\"')] | .[].'\"id\"' ' )
do
  myOCID=$ii
  echo "${myOCID}"
  if [ "${myOCID}" == "${FANKFURT_INSTANCE_OCID}" ] ; then 
    
    FANKFURT_BOOT_VOLUME_OCID=$(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"boot-volume-id\"' ' )
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Boot Volume OCID: $FANKFURT_BOOT_VOLUME_OCID"

  fi
done

fi

if [ "find block volume OCID" == "find block volume OCID" ] ; then

tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" bv volume list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  num=$(echo "${myNAME}" | grep "${FRANKFURT_BLOCK_VOLUME_NAME}" | wc -l)
  if [ $num -gt 0 ] ; then

    FANKFURT_BLOCK_VOLUME_OCID="${myOCID}"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Block Volume OCID: $myOCID"

  fi
done

if [ "${FANKFURT_BLOCK_VOLUME_OCID}" == "" ] ; then color_print "${MYcolor}" "$PF2 $FUNCNAME: after 30 sec block volume not AVAILABLE" ; fi

fi

tempfile myTEMPFILE
cat <<EOF > ${myTEMPFILE}
{
  "type": "volumeIds",
  "volume-ids": [
                  "${FANKFURT_BOOT_VOLUME_OCID}",  
                  "${FANKFURT_BLOCK_VOLUME_OCID}"
                ]
}
EOF

color_print "${MYcolor}" "$PF2 $FUNCNAME: CREATE VOLUME GROUP (without cross region replication) ${FRANKFURT_VOLUME_GROUP_NAME}"
oci --profile "${PROFILE_FRANKFURT}" bv volume-group create --availability-domain "${FRANKFURT_AVAILABILITY_DOMAIN}" \
  --compartment-id "${COMPARTMENT_OCID}"  \
  --source-details file://${myTEMPFILE} \
  --wait-for-state "AVAILABLE" \
  --display-name "${FRANKFURT_VOLUME_GROUP_NAME}" 


if [ "find volume group OCID" == "find volume group OCID" ] ; then

tempfile myTEMPFILE
oci --profile "${PROFILE_FRANKFURT}" bv volume-group list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  num=$(echo "${myNAME}" | grep "${FRANKFURT_VOLUME_GROUP_NAME}" | wc -l)
  if [ $num -gt 0 ] ; then

    FANKFURT_VOLUME_GROUP_OCID="${myOCID}"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Volume Group OCID: $myOCID"

  fi
done

fi

color_print "${MYcolor}" "$PF2 $FUNCNAME: ALTER VOLUME GROUP (set cross region replication) ${FRANKFURT_VOLUME_GROUP_NAME} and CREATE VOLUEME GROUP REPLICA ${FRANKFURT_VOLUME_GROUP_NAME}.Replica in ${PROFILE_LONDON}"

if [ "${CUSTOMER_MANAGED_KEY}" == "N" ] ; then

tempfile myTEMPFILE
cat <<EOF > ${myTEMPFILE}
[{
    "availability-domain": "${LONDON_AVAILABILITY_DOMAIN}",
    "display-name": "${FRANKFURT_VOLUME_GROUP_NAME}.Replica"
}]
EOF

color_print "${MYcolor}" "$PF2 $FUNCNAME: update volume-group - ORACLE managed key"
oci --profile "${PROFILE_FRANKFURT}" bv volume-group update --volume-group-id "${FANKFURT_VOLUME_GROUP_OCID}" \
  --volume-group-replicas file://${myTEMPFILE} \
  --wait-for-state "AVAILABLE" \
  --force
fi

if [ "${CUSTOMER_MANAGED_KEY}" == "Y" ] ; then

tempfile myTEMPFILE
cat <<EOF > ${myTEMPFILE}
[{
    "availability-domain": "${LONDON_AVAILABILITY_DOMAIN}",
    "display-name": "${FRANKFURT_VOLUME_GROUP_NAME}.Replica",
    "xrrKmsKeyId" : "${LONDON_KEY_OCID}"
}]
EOF

color_print "${MYcolor}" "$PF2 $FUNCNAME: update volume-group - CUSTOMER managed key"
oci --profile "${PROFILE_FRANKFURT}" bv volume-group update --volume-group-id "${FANKFURT_VOLUME_GROUP_OCID}" \
  --volume-group-replicas file://${myTEMPFILE} \
  --wait-for-state "AVAILABLE" \
  --force

fi
}

function activate_volume_group () {
color_print "${MYcolor}" "$PF1 function: $FUNCNAME"
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference           : https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'


if [ "is boot volume replica available" == "is boot volume replica available" ] ; then
  BOOT_VOLUME_REPLICA="NOT AVAILABLE"
  COUNTER=50
  until [ $COUNTER -lt 10 ] 
  do
    color_print "${MYcolor}" "wait 5 seconds for BOOT VOLUME REPLICA Availability"
    sleep 5
    let COUNTER=$COUNTER-1 
    tempfile myTEMPFILE
    oci --profile "${PROFILE_LONDON}" bv boot-volume-replica list --all --availability-domain "${LONDON_AVAILABILITY_DOMAIN}" --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}" 
    for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
    do
      myOCID=$ii
      myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
      num=$(echo "${myNAME}" | grep "${FRANKFURT_SERVER_NAME}" | wc -l) 
      if [ $num -gt 0 ] ; then 
        color_print "${MYcolor}" "$PF2 $FUNCNAME: BOOT VOLUME REPLICA AVAILABLE: $myNAME, $myOCID"
        echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): boot volume replica available" >> "${LOG_FILE}"
        BOOT_VOLUME_REPLICA="AVAILABLE"
        let COUNTER=0
      fi
      color_print "${MYcolor}" "$PF2 $FUNCNAME: BOOT VOLUME REPLICA AVAILABLE: $myNAME, $BOOT_VOLUME_REPLICA"
    done
  done
fi

if [ "is block volume replica available" == "is block volume replica available" ] ; then
  BLOCK_VOLUME_REPLICA="NOT AVAILABLE"
  COUNTER=50
  until [  $COUNTER -lt 10 ]; do
    color_print "${MYcolor}" "wait 5 seconds for BLOCK VOLUME REPLICA Availability"
    sleep 5
    let COUNTER=$COUNTER-1 
    tempfile myTEMPFILE
    oci --profile "${PROFILE_LONDON}" bv block-volume-replica list --all --availability-domain "${LONDON_AVAILABILITY_DOMAIN}" --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}" 
    for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
    do
      myOCID=$ii
      myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
      num=$(echo "${myNAME}" | grep "${FRANKFURT_BLOCK_VOLUME_NAME}" | wc -l) 
      if [ $num -gt 0 ] ; then 
        color_print "${MYcolor}" "$PF2 $FUNCNAME: BLOCK VOLUME REPLICA AVAILABLE: $myNAME, $myOCID"
        echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): block volume replica available" >> "${LOG_FILE}"
        BLOCK_VOLUME_REPLICA="AVAILABLE"
        let COUNTER=0
      fi
      color_print "${MYcolor}" "$PF2 $FUNCNAME: BLOCK VOLUME REPLICA AVAILABLE: $myNAME, $BLOCK_VOLUME_REPLICA"
    done 
  done
fi

if [ "is volume group replica available" == "is volume group replica available" ] ; then
  VOLUME_GROUP_REPLICA="NOT AVAILABLE"
  COUNTER=50
  until [  $COUNTER -lt 10 ]; do
    color_print "${MYcolor}" "wait 5 seconds for VOLUME GROUP REPLICA Availability"
    sleep 5
    let COUNTER=$COUNTER-1 
    tempfile myTEMPFILE
    oci --profile "${PROFILE_LONDON}" bv volume-group-replica list --all --availability-domain "${LONDON_AVAILABILITY_DOMAIN}" --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}" 
    for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
    do
      myOCID=$ii
      myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
      num=$(echo "${myNAME}" | grep "${FRANKFURT_VOLUME_GROUP_NAME}" | wc -l) 
      if [ $num -gt 0 ] ; then 
        color_print "${MYcolor}" "$PF2 $FUNCNAME: VOLUME GROUP REPLICA AVAILABLE: $myNAME, $myOCID"
        echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): volume group replica available" >> "${LOG_FILE}"
        VOLUME_GROUP_REPLICA="AVAILABLE"
        let COUNTER=0
      fi
      color_print "${MYcolor}" "$PF2 $FUNCNAME: VOLUME GROUP REPLICA AVAILABLE: $myNAME, $VOLUME_GROUP_REPLICA"
    done 
  done
fi

if [ "find volume group replica OCID" == "find volume group replica OCID" ] ; then

VOLUME_GROUP_REPLICA_OCID=""
tempfile myTEMPFILE
oci --profile "${PROFILE_LONDON}" bv volume-group-replica list --all --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${LONDON_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}"
for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  color_print "${MYcolor}" "$PF2 $FUNCNAME: FOUND Volume Group Name: $myNAME"
  num=$(echo "${myNAME}" | grep "${FRANKFURT_VOLUME_GROUP_NAME}.Replica" | wc -l)
  if [ $num -gt 0 ] ; then

    VOLUME_GROUP_REPLICA_OCID="${myOCID}"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Volume Group Replica OCID: $myOCID"

  fi
done

if [ "${CUSTOMER_MANAGED_KEY}" == "N" ] ; then

  tempfile myTEMPFILE
cat <<EOF > ${myTEMPFILE}
  {
    "type": "volumeGroupReplicaId",
    "volumeGroupReplicaId": "${VOLUME_GROUP_REPLICA_OCID}"
  }
EOF

  if [ $DEBUG_LEVEL -gt 0 ] ; then 
    color_print "${MYcolor}" "$PF2 $FUNCNAME: cat ${myTEMPFILE}"
    cat ${myTEMPFILE}
  fi

fi

if [ "${CUSTOMER_MANAGED_KEY}" == "Y" ] ; then

  tempfile myTEMPFILE
cat <<EOF > ${myTEMPFILE}
  {
    "type": "volumeGroupReplicaId",
    "volumeGroupReplicaId": "${VOLUME_GROUP_REPLICA_OCID}"
  }
EOF

  if [ $DEBUG_LEVEL -gt 0 ] ; then 
    color_print "${MYcolor}" "$PF2 $FUNCNAME: cat ${myTEMPFILE}"
    cat ${myTEMPFILE}
  fi

fi



fi

if [ "${BOOT_VOLUME_REPLICA}" == "AVAILABLE" ] && [ "${BLOCK_VOLUME_REPLICA}" == "AVAILABLE" ] && [ "${VOLUME_GROUP_REPLICA_OCID}" != "" ] ; then

  color_print "${MYcolor}" "$PF2 $FUNCNAME: WAIT (initial full data copy cross region) before ACTIVATE VOLUME GROUP REPLICA ${FRANKFURT_VOLUME_GROUP_NAME}.Replica in ${PROFILE_LONDON} => CREATE VOLUME GROUP ${LONDON_VOLUME_GROUP_NAME}"
  echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): WAIT 10 Minutes (initial full data copy cross region) before ACTIVATE VOLUME GROUP REPLICA ${FRANKFURT_VOLUME_GROUP_NAME}.Replica in ${PROFILE_LONDON} => CREATE VOLUME GROUP ${LONDON_VOLUME_GROUP_NAME}" >> "${LOG_FILE}"
  sleep ${WAIT_BEFORE_ACTIVATE}
  color_print "${MYcolor}" "$PF2 $FUNCNAME: ACTIVATE VOLUME GROUP REPLICA ${FRANKFURT_VOLUME_GROUP_NAME}.Replica in ${PROFILE_LONDON} => CREATE VOLUME GROUP ${LONDON_VOLUME_GROUP_NAME}"
  echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): start volume group replica activation" >> "${LOG_FILE}"
  oci --profile "${PROFILE_LONDON}" bv volume-group create --availability-domain "${LONDON_AVAILABILITY_DOMAIN}" \
  --compartment-id "${COMPARTMENT_OCID}"  \
  --source-details file://${myTEMPFILE} \
  --wait-for-state "AVAILABLE" \
  --display-name "${LONDON_VOLUME_GROUP_NAME}" 
  echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): volume group replica activation done" >> "${LOG_FILE}"
  
  if [ "is boot volume in london available" == "is boot volume in london available" ] ; then
    BOOT_VOLUME_IN_LONDON="NOT AVAILABLE"
    COUNTER=50
    until [ $COUNTER -lt 10 ] 
    do
      color_print "${MYcolor}" "wait 5 seconds for BOOT VOLUME Availability in london"
      sleep 5
      let COUNTER=$COUNTER-1 
      tempfile myTEMPFILE
      oci --profile "${PROFILE_LONDON}" bv boot-volume list --all --availability-domain "${LONDON_AVAILABILITY_DOMAIN}" --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}" 
      for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
      do
        myOCID=$ii
        myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
        num=$(echo "${myNAME}" | grep "${FRANKFURT_SERVER_NAME}" | wc -l) 
        if [ $num -gt 0 ] ; then 
          color_print "${MYcolor}" "$PF2 $FUNCNAME: BOOT VOLUME AVAILABLE: $myNAME, $myOCID"
          echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): boot volume in london available" >> "${LOG_FILE}"
          BOOT_VOLUME_IN_LONDON="AVAILABLE"
          BOOT_VOLUME_OCID_IN_LONDON="${myOCID}"
          let COUNTER=0

if [ "${CUSTOMER_MANAGED_KEY}" == "Y" ] ; then		  
 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: update instance (boot volume ${BOOT_VOLUME_OCID_IN_LONDON}) - CUSTOMER managed key"
      oci --profile "${PROFILE_LONDON}" bv boot-volume-kms-key update \
          --boot-volume-id "${BOOT_VOLUME_OCID_IN_LONDON}"            \
          --kms-key-id "${LONDON_KEY_OCID}"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: update instance (boot volume ${BOOT_VOLUME_OCID_IN_LONDON}) - CUSTOMER managed key DONE, wait 20s"
      sleep 20

fi


        fi
        color_print "${MYcolor}" "$PF2 $FUNCNAME: BOOT VOLUME in london AVAILABLE: $myNAME, $BOOT_VOLUME_IN_LONDON"
     done
    done
  fi

  # create instance
  if [ "${BOOT_VOLUME_IN_LONDON}" == "AVAILABLE" ] ; then

tempfile myTEMPFILE1
cat <<EOF > ${myTEMPFILE1}
{
  "areAllPluginsDisabled": false,
  "isManagementDisabled": false,
  "isMonitoringDisabled": false,
  "pluginsConfig": [
    {
      "desiredState": "ENABLED",
      "name": "Block Volume Management"
    }
  ]
}
EOF


oci --profile "${PROFILE_LONDON}" compute instance launch --availability-domain "${LONDON_AVAILABILITY_DOMAIN}" \
 --compartment-id      "${COMPARTMENT_OCID}" \
 --shape               "${INSTANCE_SHAPE_NAME}" \
 --shape-config        "${INSTANCE_CONFIG_SHAPE}" \
 --subnet-id           "${LONDON_SUBNET_OCID}" \
 --display-name        "${LONDON_SERVER_NAME}" \
 --source-boot-volume-id "${BOOT_VOLUME_OCID_IN_LONDON}" \
 --assign-public-ip    "true" \
 --agent-config        file://${myTEMPFILE1} \
 --wait-for-state      "RUNNING" \
 --ssh-authorized-keys-file "${PUBLIC_SSH_KEY}"

color_print "${MYcolor}" "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): new instance in london AVAILABLE" 
echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): new instance in london AVAILABLE" >> "${LOG_FILE}"


color_print "${MYcolor}" "$PF2 $FUNCNAME: wait 10s"
sleep 10

   
tempfile myTEMPFILE2
oci --profile "${PROFILE_LONDON}" bv volume list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE2}"
for ii in $(cat "${myTEMPFILE2}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
do
  myOCID=$ii
  myNAME=$( cat "${myTEMPFILE2}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  if [ $DEBUG_LEVEL -gt 0 ] ; then 
    color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume OCID: $myOCID"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume Name: $myNAME"
  fi
  
  num=$(echo "${myNAME}" | grep "${FRANKFURT_BLOCK_VOLUME_NAME}" | wc -l)
  
  if [ $num -gt 0 ] ; then 
    
    TMP_BLOCK_VOLUME_OCID=$myOCID
    color_print "${MYcolor}" "$PF2 $FUNCNAME: Block Volume in ${PROFILE_LONDON} found: $myNAME"

if [ "${CUSTOMER_MANAGED_KEY}" == "Y" ] ; then		  
 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: update block volume ${TMP_BLOCK_VOLUME_OCID}) - CUSTOMER managed key"
      oci --profile "${PROFILE_LONDON}" bv volume-kms-key update \
          --volume-id "${TMP_BLOCK_VOLUME_OCID}"            \
          --kms-key-id "${LONDON_KEY_OCID}"

      color_print "${MYcolor}" "$PF2 $FUNCNAME: wait 20s"
      sleep 20

fi

  fi
  
done


for MYPROFILE in $( echo "${PROFILE_LONDON}" )
do
  tempfile myTEMPFILE3
  oci --profile "${MYPROFILE}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE3}"
  for ii in $(cat "${myTEMPFILE3}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE3}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ "${myNAME}" == "${LONDON_SERVER_NAME}" ] && [ "${MYPROFILE}" == "${PROFILE_LONDON}" ] ; then 
      TMP_INSTANCE_OCID=$myOCID  
    fi
  done
done

color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume OCID: $TMP_BLOCK_VOLUME_OCID"
color_print "${MYcolor}" "$PF2 $FUNCNAME: instance OCID: $TMP_INSTANCE_OCID"

color_print "${MYcolor}" "$PF2 $FUNCNAME: ATTACH BLOCK VOLUME TO INSTANCE"
echo "${PF2} ($(date "+%d.%m.%Y %H:%M:%S")): attache block volume to instance in london" >> "${LOG_FILE}"
oci --profile "${PROFILE_LONDON}" compute volume-attachment attach-iscsi-volume \
 --instance-id                        "$TMP_INSTANCE_OCID" \
 --volume-id                          "$TMP_BLOCK_VOLUME_OCID" \
 --device                             "/dev/oracleoci/oraclevdb" \
 --is-agent-auto-iscsi-login-enabled  "YES" \
 --wait-for-state                     "ATTACHED"
        
  else
    color_print "${MYcolor}" "$PF2 $FUNCNAME: CREATE INSTANCE in ${PROFILE_LONDON} NOT POSSIBLE"
  fi

  
else

  color_print "${MYcolor}" "$PF2 $FUNCNAME: BOOT VOLUME REPLICA NOT AVAILABLE"
 
fi

}

fi

# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------
