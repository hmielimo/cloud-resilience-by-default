#!/bin/bash
# Author: Heinz Mielimonka
# Version: @(#).parameter.sh (c) Heinz Mielimonka
#
#@  Prepare and run Cross Region Replication.
#@
#
# Update history:
#
# V 1.1.0 15.10.2024 add customer managed key
# V 1.0.0 21.07.2022 New version
#





# ---------------------------------------------------------------------------------------------------------------------------------------------
# CUSTOMER SPECIFIC VALUES - please update appropriate
# ---------------------------------------------------------------------------------------------------------------------------------------------
export TENENCY_OCID=[please insert your value here]                 # Tenancy Name: 
export USER_OCID=[please insert your value here]                    # User Name: 
export COMPARTMENT_OCID=[please insert your value here]             # Compartment Name: 
export FRANKFURT_SUBNET_OCID=[please insert your value here]        # Subnet Name: Public Subnet in Region Frankfurt 
export LONDON_SUBNET_OCID=[please insert your value here]           # Subnet Name: Public Subnet in Region London 
export CUSTOMER_MANAGED_KEY=[please insert your value here]         # Customer manaeged Key [Y/N]
export FRANKFURT_KEY_OCID=[please insert your value here]           # Key OCID: Customer manaeged Key OCID in Region Frankfurt 
export LONDON_KEY_OCID=[please insert your value here]              # Key OCID: Customer manaeged Key OCID in Region London 
export FINGERPRINT=[please insert your value here]                  # API fingerprint
export PUBLIC_SSH_KEY="/home/opc/.ssh/id_rsa.pub"

# ---------------------------------------------------------------------------------------------------------------------------------------------
# VALUES (normally do not touch)
# ---------------------------------------------------------------------------------------------------------------------------------------------
export LOG_FILE="/home/opc/environment.log"                                                                             # 
export WAIT_BEFORE_ACTIVATE=600                                                                                          # wait before activate in seconds
export DEBUG_LEVEL=0                                                                                                    # 0 (off); 1 (low); 2 (high)
export INSTANCE_SHAPE_NAME=VM.Standard.E4.Flex                                                                          # Shapes https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm
#export INSTANCE_CONFIG_SHAPE='{ "memoryInGBs": 16, "ocpus": 1 }'                                                       # Shapes https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm
export INSTANCE_CONFIG_SHAPE='{ "memoryInGBs": 256, "ocpus": 16 }'                                                      # Shapes https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm
export IMAGE_FRANKFURT_OCID=ocid1.image.oc1.eu-frankfurt-1.aaaaaaaapsgbubgar2niu3nidkkdabey44g2da6gwgl3mzvwggaiccdhwsfq # All Oracle Linux 9.x Images https://docs.oracle.com/en-us/iaas/images/oracle-linux-9x/
export IMAGE_LONDON_OCID=ocid1.image.oc1.uk-london-1.aaaaaaaanpiqglmxtvzvjxsppx7zdxv5pamrhvbvs332cdoetxo5rpbl453q       # All Oracle Linux 9.x Images https://docs.oracle.com/en-us/iaas/images/oracle-linux-9x/
export BACKUP_POLICY=myBackupPolicy                                                                                     # Backup Policy Name
export AD=1
export AD_PREFIX=fyxu                                                                                                   # Prifix of Availability Domain

export FRANKFURT_REGION_IDENTIFIER=eu-frankfurt-1                                                                       # 
export FRANKFURT_SERVER_NAME=ServerFrankfurt                                                                            #
export FRANKFURT_BLOCK_VOLUME_NAME=BlockVolumeFrankfurt                                                                 #
export FRANKFURT_VOLUME_GROUP_NAME=VolumeGroup.Frankfurt                                                                #
export FRANKFURT_AVAILABILITY_DOMAIN="${AD_PREFIX}:${FRANKFURT_REGION_IDENTIFIER}-AD-${AD}"                             # fyxu:[Region Identifier]-AD-[Number of Availability Domain] see https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export FRANKFURT_VOLUME_GROUP_SOURCE_DETAILS='{ "memoryInGBs": 16, "ocpus": 1 }'                                        # Shapes https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm
export PROFILE_FRANKFURT=FRANKFURT                                                                                      # oci profile frankfurt

export LONDON_REGION_IDENTIFIER=uk-london-1                                                                             # 
export LONDON_SERVER_NAME=ServerLondon                                                                                  #
export LONDON_BLOCK_VOLUME_NAME=BlockVolumeLondon                                                                       #
export LONDON_VOLUME_GROUP_NAME=VolumeGroup.London                                                                      #
export LONDON_AVAILABILITY_DOMAIN="${AD_PREFIX}:${LONDON_REGION_IDENTIFIER}-AD-${AD}"                                   # fyxu:[Region Identifier]-AD-[Number of Availability Domain] see https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export PROFILE_LONDON=LONDON                                                                                            # oci profile london

export PF1=###                                                                                                          # Prefix 1 - used to introduce function
export PF2="${PF1}.###:"                                                                                                # Prefix 2 - used to log informations inside functions
export MYcolor="${IYellow}"                                                                                             # define output color
if [ ${DEBUG_LEVEL} -eq 0 ] ; then export MYcolor=$ICyan   ; fi
if [ ${DEBUG_LEVEL} -eq 1 ] ; then export MYcolor=$IPurple ; fi
if [ ${DEBUG_LEVEL} -eq 2 ] ; then export MYcolor=$IRed    ; fi
# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------
