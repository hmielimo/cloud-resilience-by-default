#!/bin/bash
# Author: Heinz Mielimonka
# Version: @(#).demonstrate.server.resilience.by.default.sh (c) Heinz Mielimonka
#
#@  Prepare and run Cross Region Replication.
#@
#
# Update history:
#
# V 1.1.0 15.10.2024 add customer managed key
# V 1.0.0 21.07.2022 New version
#


# ---------------------------------------------------------------------------------------------------------------------------------------------
# prepare environement (load functions)
# ---------------------------------------------------------------------------------------------------------------------------------------------

source $(dirname "$0")/functions0.sh

# ---------------------------------------------------------------------------------------------------------------------------------------------
# Oracle Cloud Infrastructure CLI Command Reference - https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/
# ---------------------------------------------------------------------------------------------------------------------------------------------

MYCOUNT=0 && MYOUTPUT="Show environment details" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

rm -f "${LOG_FILE}"
PREFIX="### "
PREFIX2="###.### "
echo "#################################################################################################################################################" >> "${LOG_FILE}"
echo "${PREFIX} INITIAL" >> "${LOG_FILE}"

show_environment

fi

MYOUTPUT="Cleanup environment (takes about 3 minutes)" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

delete_environment

fi

MYOUTPUT="Show environment details" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

PREFIX="### "
PREFIX2="###.### "
echo "#################################################################################################################################################" >> "${LOG_FILE}"
echo "${PREFIX} AFTER CLEANUP" >> "${LOG_FILE}"

show_environment

fi

MYOUTPUT="Create environment (takes about 2 minutes)" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

create_instance
create_block_volume
create_volume_group

fi

MYOUTPUT="Show environment details" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

PREFIX="### "
PREFIX2="###.### "
echo "#################################################################################################################################################" >> "${LOG_FILE}"
echo "${PREFIX} AFTER CREATE" >> "${LOG_FILE}"

show_environment

fi

MYOUTPUT="Ask customer to configure new server in FRANKFURT" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

color_print "${IGreen}" "**********************************************************************************************************************"
color_print "${IGreen}" "**********************************************************************************************************************"
color_print "${IGreen}" ">>> "
color_print "${IGreen}" ">>> "
color_print "${IGreen}" ">>> "

read -p '>>> Please configure your Server in Region FRANKFURT. If you are ready - please type done here: ' configserver
if [ "${configserver}" == "done" ] ; then
    echo -e "\nOK I will go on and activate Volume Group Replica in target region"
else
    echo -e "\nOK I stop my work"
	exit
fi
fi

MYOUTPUT="Activate Volume Group Replica and create new server in target region (takes about 4 minutes)" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

activate_volume_group

fi

MYOUTPUT="Show environment details" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

PREFIX="### "
PREFIX2="###.### "
echo "#################################################################################################################################################" >> "${LOG_FILE}"
echo "${PREFIX} AFTER ACTIVATE" >> "${LOG_FILE}"

show_environment

fi

MYOUTPUT="Ask customer to test new server in LONDON" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

color_print "${IGreen}" "**********************************************************************************************************************"
color_print "${IGreen}" "**********************************************************************************************************************"
color_print "${IGreen}" ">>> "
color_print "${IGreen}" ">>> "
color_print "${IGreen}" ">>> "

read -p '>>> Please test your Server in Region LONDON. If you are ready - please type done here: ' configserver
if [ "${configserver}" == "done" ] ; then
    echo -e "\nOK I will go on"
else
    echo -e "\nOK I stop my work"
	exit
fi
fi

MYOUTPUT="Cleanup environment" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

delete_environment

fi

MYOUTPUT="Show environment details" && MYCOUNT=$(($MYCOUNT + 1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

PREFIX="### "
PREFIX2="###.### "
echo "#################################################################################################################################################" >> "${LOG_FILE}"
echo "${PREFIX} AFTER CLEANUP" >> "${LOG_FILE}"

show_environment

fi

MYOUTPUT="End of Programm" && MYCOUNT=$(($MYCOUNT + 1)) 
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"
# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------